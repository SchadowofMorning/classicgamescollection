
import * as express from 'express';
const app = express();
import * as ws from 'ws';
import * as r from 'rethinkdb';
import * as fs from 'fs';
const WebSocketServer = ws.Server;

var config = JSON.parse(fs.readFileSync('./config.json').toString());

var clientlist = [];

import Client from './classes/Client';

function Connect(config, cb) {
  r.connect(config, (err, connection) => {
      if(err) console.log(err);
      cb(connection);
  });
}
Connect(config.database, (connection) => {
   var wsserver = new WebSocketServer(config.wss);

   wsserver.on('connection', (socket) => {
     console.log("client connected");
     clientlist.push(new Client(connection, socket));
   });
   app.use('/', express.static('public'));
   app.listen(config.http.port, () => {
     console.log("server listening for active connections on *:" + config.wss.port);
   });
   setInterval(() => {
    clientlist.forEach((client: Client, index: number) => {
      if(client.socket.readyState == 3){
        client.close();
        if(client.user){
          console.log(client.user.id + " timed out");
        }
        clientlist.splice(index, 1);
      }
    });
   }, 5000);
 });
