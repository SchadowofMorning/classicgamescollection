"use strict";
import * as r from 'rethinkdb';
import Game from './Game';
export default class Lobby {
    public id: string;
    public title: string;
    public users: Array<string>;
    public type: number;
    public running: boolean;
    constructor(data){
        this.id = data.id;
        this.users = data.users;
        this.title = data.title;
        this.type = data.type;
        this.running = data.running;
    }
    add_user(id, connection, callback){
        this.users.push(id);
        r.db('main').table('lobbys').get(this.id).update(this).run(connection, callback);
    }
    remove_user(id, connection, callback){
        this.users.splice(this.users.indexOf(id), 1);
        if(this.users.length > 0)
            r.db('main').table('lobbys').get(this.id).update(this).run(connection, callback);
        else
            r.db('main').table('lobbys').get(this.id).delete().run(connection, callback);
    }
    start_game(connection: r.Connection, callback: (string) => void){
        this.running = true;
        let game = Game.create(this.id, this.users, this.type, connection, (id) => {
            callback(id);
        });
        r.db('main').table('lobbys').get(this.id).update(this).run(connection, (err, res) => { if(err) throw err; });
        return game;
    }
    static create(data, connection, cb){
        r.db('main').table('lobbys').insert({title: data.title, users: data.users, type: data.type, running: false}).run(connection, (err, res) => {
            if(err) throw err;
            cb(res.generated_keys[0]);
        });
    }
    static load(id, connection, cb){
        r.db('main').table('lobbys').get(id).run(connection, (err, res) => {
            if(err) throw err;
            cb(new Lobby(res));
        });
    }
    static query(filter, connection, cb){
        r.db('main').table('lobbys').filter(filter).run(connection, (err, res) => {
            if(err) throw err;
            res.toArray().then((value) => {
                cb(value);
            });
        });
    }
    static onChange(id: string, connection: r.Connection, then: (item) => void, cb: (cursor: r.Cursor) => void){
        r.db('main').table('lobbys').filter({id: id}).changes().run(connection, (err: Error, cursor: r.Cursor) => {
            if(err) throw err;
            cursor.each((err, element) => {
                if(err) console.log(err);
                then(element["new_val"]);
            });
            console.log("changefeed opened for lobby: " + id);
            cb(cursor);
        });
    }
    static onListChange(filter: any, connection: r.Connection, then: (change) => void, cb:(cursor: r.Cursor) => void){
        r.db('main').table('lobbys').changes(filter).run(connection, (err: Error, cursor: r.Cursor) => {
            if(err) throw err;
            cursor.each((err : Error, row: any) => {
                if(err) console.log(err);
                then(row);
            });
            console.log("changefeed opened for lobbys");
            cb(cursor);
        });
    }
    static get(id, connection, then){
        r.db('main').table('lobbys').get(id).run(connection, (err, res) => { if(err) console.log(err); then(new Lobby(res)); });
    }
}