import * as r from 'rethinkdb';

export default class Game {
    id: string;
    users: Array<string>;
    type: number;
    turns: Array<string>;

    constructor(data: any){
        this.id = data.id;
        this.users = data.users;
        this.type = data.type;
        this.turns = data.turns;
    }


    static create(id: string, users: Array<string>, type: number, connection: r.Connection, callback: (string) => any){
        let data = {id: id, users: users, type: type, turns: []};
        let game = new Game(data);
        r.db('main').table('games').insert(game).run(connection, (err, res) => {
            if(err) throw err;
            callback(game.id);
        });
        return game;
    }

    static onChange(id: string, connection: r.Connection, then:(any) => any, cb: (cursor: r.Cursor) => void){
        r.db('main').table('games').filter({id: id}).changes().run(connection, (err, cursor) => {
            if(err) console.log(err);
            cursor.each((err : Error, element: any) => {
                if(err) console.log(err);
                console.log("moved");
                then(element);
            });
            console.log("changefeed opened for game: " + id);
            cb(cursor);
        });
    }
    static query(id: string, connection: r.Connection, then:(any) => any){
        r.db('main').table('games').get(id).run(connection, (err, res) => {
            if(err) console.log(err);
            then(res);
        });
    }
    update(data, connection){
        if(data == null){
            this.turns = [];
        } else {
            if(data.turns){
                this.turns = data.turns;
            } else {
                this.turns = data;
            }
        }
        console.log(this);
        r.db('main').table('games').get(this.id).update(this).run(connection, (err, res) => {
            if(err) throw err;
        });
    }
}