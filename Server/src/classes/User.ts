"use strict";
/**
 * User class handling all of the users requests
 */
import * as r from 'rethinkdb';
import { RSA_NO_PADDING } from 'constants';
export default class User {
    public id: string;
    public password: string;

    public constructor(id : string, password : string, connection, cb){
        this.id = id;
        this.password = password;
        r.db("main").table("users").get(id).run(connection, (err, res) => {
            if(err) console.log(err);
            if(res){
                if(res['password'] == ""){
                    r.db("main").table("users").get(id).update({password: password}).run(connection, (err, res) => {
                        if(err) throw err;
                        cb(true);
                    });
                } else if(res['password'] == password){
                    r.db("main").table("users").get(id).update({online: true}).run(connection, (err, res) => { if(err) throw err; cb(true); });
                } else {
                    cb(false);
                }
            } else {
                r.db("main").table("users").insert({id: id, password: password, online: true}).run(connection, (err, res) => {
                    if(err) throw err;
                    cb(true);
                });
            }
        });
    }
}