'use strict';
let debug = true;
import User from './User';
import Lobby from './Lobby';
import Game from './Game';
export default class Client {
    socket:any;
    connection: any;
    callbacks: Array<Array<Function>>;
    user: User;
    cursor: any;
    lobby: Lobby;
    game: Game;
    constructor(connection, socket){
        this.socket = socket;
        this.connection = connection;
        this.callbacks = [];
        socket.on("message", (msg) => {
            this.handle(msg);
        });
        socket.on("close", () => {
            this.handle(JSON.stringify({type: "close", data: "close"}));
        });
        this.on("authentication", (data) => {
            this.user = new User(data.id, data.password, connection, (result) => {
                if(result) this.successAuthBinding();
                this.send("auth-response", result);
            });
        });
        this.on("close", (data) => {
            this.close();
        });
    }

    successAuthBinding(){
        this.on('get-lobbys', (filter) => {
            this.getLobbys(filter);
            this.listenForLobbys();
        });
        this.on('create-lobby', (data) => {
            Lobby.create(data, this.connection, (id) => {
                console.log(id);
                this.send('created-lobby', id);
            });
        });
        this.on('join-lobby', (id) => {
            if(this.cursor) this.cursor.close();

            Lobby.onChange(id, this.connection, (change) => {
                console.log("changed");
                this.send('lobby-update', JSON.stringify(change));
            },(cursor) => { 
                this.cursor = cursor;
            });
            Lobby.get(id, this.connection, (lobby) => {
                this.lobby = lobby;
                lobby.add_user(this.user.id, this.connection, (err, res) => { if(err) throw err; });
                this.send('lobby-update', JSON.stringify(lobby));
            });
        });
        this.on('start-lobby', (id) => {
            if(this.cursor) this.cursor.close();
            this.lobby.start_game(this.connection, (id) => {
                this.send('join-game', id);
            });
        });
        this.on('leave-lobby', (id) => {
            this.lobby.remove_user(this.user.id, this.connection, (err, res) => { if(err) throw err; });
            if(this.cursor) this.cursor.close();
            this.send("leave-lobby", '""');
        });
        this.on('join-game', (id) => {
            Game.onChange(id, this.connection, (change) => {
                console.log(change);
                this.send('game-update', JSON.stringify(change));
            }, (cursor) => { this.cursor = cursor;});
            console.log(this.cursor);
            Game.query(id, this.connection, (game) => {
                this.game = new Game(game);
            });
            this.on('game-update', (data) => {
                console.log("updated");
                console.log(data);
                this.game.update(data, this.connection);
            });
            this.on('end-game', (data) => {
                console.log("game:" + this.game.id);
                this.cursor.close();
                this.send("end-game", data);
            });
        });
    }
    getLobbys(filter){
        Lobby.query(filter, this.connection, (res) => {
            this.send("lobby-data", JSON.stringify(res));
        });
    }
    listenForLobbys(){
        if(this.cursor)this.cursor.close();
        Lobby.onListChange({}, this.connection, (change) => {
            this.send('lobby-changes', JSON.stringify(change));
        }, (cursor) => { this.cursor = cursor; })
    }
    on(type : string, cb: (string) => void){
        if(this.callbacks[type]){
            this.callbacks[type].push(cb);
        } else {
            this.callbacks[type] = [cb];
        }
    }
    handle(message: string){
        if(debug)console.log(message);
        let msg = JSON.parse(message);
        let type = msg["type"];
        let data = msg["data"];
        for(var index in this.callbacks){
            if(index == type){
                for(var item in this.callbacks[index]){
                    this.callbacks[index][item](data);
                }
            }
        }
    }
    
    send(type, data){
        if(this.socket.readyState == 3){
            this.close();
        } else {
            this.socket.send(JSON.stringify({type: type, data: data}));
        }
    }
    close(){
        if(this.cursor)this.cursor.close();
        if(this.lobby){
            this.lobby.remove_user(this.user.id, this.connection, () => {
                this.socket.close();
            });
        } else {
            this.socket.close();
            if(this.cursor)this.cursor.close();
        }
    }
}