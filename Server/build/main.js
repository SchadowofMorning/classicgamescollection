"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var express = require("express");
var app = express();
var ws = require("ws");
var r = require("rethinkdb");
var fs = require("fs");
var WebSocketServer = ws.Server;
var config = JSON.parse(fs.readFileSync('./config.json').toString());
var clientlist = [];
var Client_1 = require("./classes/Client");
function Connect(config, cb) {
    r.connect(config, function (err, connection) {
        if (err)
            console.log(err);
        cb(connection);
    });
}
Connect(config.database, function (connection) {
    var wsserver = new WebSocketServer(config.wss);
    wsserver.on('connection', function (socket) {
        console.log("client connected");
        clientlist.push(new Client_1.default(connection, socket));
    });
    app.use('/', express.static('public'));
    app.listen(config.http.port, function () {
        console.log("server listening for active connections on *:" + config.wss.port);
    });
    setInterval(function () {
        clientlist.forEach(function (client, index) {
            if (client.socket.readyState == 3) {
                client.close();
                if (client.user) {
                    console.log(client.user.id + " timed out");
                }
                clientlist.splice(index, 1);
            }
        });
    }, 5000);
});
//# sourceMappingURL=main.js.map