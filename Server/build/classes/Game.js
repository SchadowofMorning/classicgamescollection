"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var r = require("rethinkdb");
var Game = /** @class */ (function () {
    function Game(data) {
        this.id = data.id;
        this.users = data.users;
        this.type = data.type;
        this.turns = data.turns;
    }
    Game.create = function (id, users, type, connection, callback) {
        var data = { id: id, users: users, type: type, turns: [] };
        var game = new Game(data);
        r.db('main').table('games').insert(game).run(connection, function (err, res) {
            if (err)
                throw err;
            callback(game.id);
        });
        return game;
    };
    Game.onChange = function (id, connection, then, cb) {
        r.db('main').table('games').filter({ id: id }).changes().run(connection, function (err, cursor) {
            if (err)
                console.log(err);
            cursor.each(function (err, element) {
                if (err)
                    console.log(err);
                console.log("moved");
                then(element);
            });
            console.log("changefeed opened for game: " + id);
            cb(cursor);
        });
    };
    Game.query = function (id, connection, then) {
        r.db('main').table('games').get(id).run(connection, function (err, res) {
            if (err)
                console.log(err);
            then(res);
        });
    };
    Game.prototype.update = function (data, connection) {
        if (data == null) {
            this.turns = [];
        }
        else {
            if (data.turns) {
                this.turns = data.turns;
            }
            else {
                this.turns = data;
            }
        }
        console.log(this);
        r.db('main').table('games').get(this.id).update(this).run(connection, function (err, res) {
            if (err)
                throw err;
        });
    };
    return Game;
}());
exports.default = Game;
//# sourceMappingURL=Game.js.map