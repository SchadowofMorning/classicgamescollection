"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var r = require("rethinkdb");
var Game_1 = require("./Game");
var Lobby = /** @class */ (function () {
    function Lobby(data) {
        this.id = data.id;
        this.users = data.users;
        this.title = data.title;
        this.type = data.type;
        this.running = data.running;
    }
    Lobby.prototype.add_user = function (id, connection, callback) {
        this.users.push(id);
        r.db('main').table('lobbys').get(this.id).update(this).run(connection, callback);
    };
    Lobby.prototype.remove_user = function (id, connection, callback) {
        this.users.splice(this.users.indexOf(id), 1);
        if (this.users.length > 0)
            r.db('main').table('lobbys').get(this.id).update(this).run(connection, callback);
        else
            r.db('main').table('lobbys').get(this.id).delete().run(connection, callback);
    };
    Lobby.prototype.start_game = function (connection, callback) {
        this.running = true;
        var game = Game_1.default.create(this.id, this.users, this.type, connection, function (id) {
            callback(id);
        });
        r.db('main').table('lobbys').get(this.id).update(this).run(connection, function (err, res) { if (err)
            throw err; });
        return game;
    };
    Lobby.create = function (data, connection, cb) {
        r.db('main').table('lobbys').insert({ title: data.title, users: data.users, type: data.type, running: false }).run(connection, function (err, res) {
            if (err)
                throw err;
            cb(res.generated_keys[0]);
        });
    };
    Lobby.load = function (id, connection, cb) {
        r.db('main').table('lobbys').get(id).run(connection, function (err, res) {
            if (err)
                throw err;
            cb(new Lobby(res));
        });
    };
    Lobby.query = function (filter, connection, cb) {
        r.db('main').table('lobbys').filter(filter).run(connection, function (err, res) {
            if (err)
                throw err;
            res.toArray().then(function (value) {
                cb(value);
            });
        });
    };
    Lobby.onChange = function (id, connection, then, cb) {
        r.db('main').table('lobbys').filter({ id: id }).changes().run(connection, function (err, cursor) {
            if (err)
                throw err;
            cursor.each(function (err, element) {
                if (err)
                    console.log(err);
                then(element["new_val"]);
            });
            console.log("changefeed opened for lobby: " + id);
            cb(cursor);
        });
    };
    Lobby.onListChange = function (filter, connection, then, cb) {
        r.db('main').table('lobbys').changes(filter).run(connection, function (err, cursor) {
            if (err)
                throw err;
            cursor.each(function (err, row) {
                if (err)
                    console.log(err);
                then(row);
            });
            console.log("changefeed opened for lobbys");
            cb(cursor);
        });
    };
    Lobby.get = function (id, connection, then) {
        r.db('main').table('lobbys').get(id).run(connection, function (err, res) { if (err)
            console.log(err); then(new Lobby(res)); });
    };
    return Lobby;
}());
exports.default = Lobby;
//# sourceMappingURL=Lobby.js.map