"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * User class handling all of the users requests
 */
var r = require("rethinkdb");
var User = /** @class */ (function () {
    function User(id, password, connection, cb) {
        this.id = id;
        this.password = password;
        r.db("main").table("users").get(id).run(connection, function (err, res) {
            if (err)
                console.log(err);
            if (res) {
                if (res['password'] == "") {
                    r.db("main").table("users").get(id).update({ password: password }).run(connection, function (err, res) {
                        if (err)
                            throw err;
                        cb(true);
                    });
                }
                else if (res['password'] == password) {
                    r.db("main").table("users").get(id).update({ online: true }).run(connection, function (err, res) { if (err)
                        throw err; cb(true); });
                }
                else {
                    cb(false);
                }
            }
            else {
                r.db("main").table("users").insert({ id: id, password: password, online: true }).run(connection, function (err, res) {
                    if (err)
                        throw err;
                    cb(true);
                });
            }
        });
    }
    return User;
}());
exports.default = User;
//# sourceMappingURL=User.js.map