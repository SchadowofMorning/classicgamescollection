'use strict';
Object.defineProperty(exports, "__esModule", { value: true });
var debug = true;
var User_1 = require("./User");
var Lobby_1 = require("./Lobby");
var Game_1 = require("./Game");
var Client = /** @class */ (function () {
    function Client(connection, socket) {
        var _this = this;
        this.socket = socket;
        this.connection = connection;
        this.callbacks = [];
        socket.on("message", function (msg) {
            _this.handle(msg);
        });
        socket.on("close", function () {
            _this.handle(JSON.stringify({ type: "close", data: "close" }));
        });
        this.on("authentication", function (data) {
            _this.user = new User_1.default(data.id, data.password, connection, function (result) {
                if (result)
                    _this.successAuthBinding();
                _this.send("auth-response", result);
            });
        });
        this.on("close", function (data) {
            _this.close();
        });
    }
    Client.prototype.successAuthBinding = function () {
        var _this = this;
        this.on('get-lobbys', function (filter) {
            _this.getLobbys(filter);
            _this.listenForLobbys();
        });
        this.on('create-lobby', function (data) {
            Lobby_1.default.create(data, _this.connection, function (id) {
                console.log(id);
                _this.send('created-lobby', id);
            });
        });
        this.on('join-lobby', function (id) {
            if (_this.cursor)
                _this.cursor.close();
            Lobby_1.default.onChange(id, _this.connection, function (change) {
                console.log("changed");
                _this.send('lobby-update', JSON.stringify(change));
            }, function (cursor) {
                _this.cursor = cursor;
            });
            Lobby_1.default.get(id, _this.connection, function (lobby) {
                _this.lobby = lobby;
                lobby.add_user(_this.user.id, _this.connection, function (err, res) { if (err)
                    throw err; });
                _this.send('lobby-update', JSON.stringify(lobby));
            });
        });
        this.on('start-lobby', function (id) {
            if (_this.cursor)
                _this.cursor.close();
            _this.lobby.start_game(_this.connection, function (id) {
                _this.send('join-game', id);
            });
        });
        this.on('leave-lobby', function (id) {
            _this.lobby.remove_user(_this.user.id, _this.connection, function (err, res) { if (err)
                throw err; });
            if (_this.cursor)
                _this.cursor.close();
            _this.send("leave-lobby", '""');
        });
        this.on('join-game', function (id) {
            Game_1.default.onChange(id, _this.connection, function (change) {
                console.log(change);
                _this.send('game-update', JSON.stringify(change));
            }, function (cursor) { _this.cursor = cursor; });
            console.log(_this.cursor);
            Game_1.default.query(id, _this.connection, function (game) {
                _this.game = new Game_1.default(game);
            });
            _this.on('game-update', function (data) {
                console.log("updated");
                console.log(data);
                _this.game.update(data, _this.connection);
            });
            _this.on('end-game', function (data) {
                console.log("game:" + _this.game.id);
                _this.cursor.close();
                _this.send("end-game", data);
            });
        });
    };
    Client.prototype.getLobbys = function (filter) {
        var _this = this;
        Lobby_1.default.query(filter, this.connection, function (res) {
            _this.send("lobby-data", JSON.stringify(res));
        });
    };
    Client.prototype.listenForLobbys = function () {
        var _this = this;
        if (this.cursor)
            this.cursor.close();
        Lobby_1.default.onListChange({}, this.connection, function (change) {
            _this.send('lobby-changes', JSON.stringify(change));
        }, function (cursor) { _this.cursor = cursor; });
    };
    Client.prototype.on = function (type, cb) {
        if (this.callbacks[type]) {
            this.callbacks[type].push(cb);
        }
        else {
            this.callbacks[type] = [cb];
        }
    };
    Client.prototype.handle = function (message) {
        if (debug)
            console.log(message);
        var msg = JSON.parse(message);
        var type = msg["type"];
        var data = msg["data"];
        for (var index in this.callbacks) {
            if (index == type) {
                for (var item in this.callbacks[index]) {
                    this.callbacks[index][item](data);
                }
            }
        }
    };
    Client.prototype.send = function (type, data) {
        if (this.socket.readyState == 3) {
            this.close();
        }
        else {
            this.socket.send(JSON.stringify({ type: type, data: data }));
        }
    };
    Client.prototype.close = function () {
        var _this = this;
        if (this.cursor)
            this.cursor.close();
        if (this.lobby) {
            this.lobby.remove_user(this.user.id, this.connection, function () {
                _this.socket.close();
            });
        }
        else {
            this.socket.close();
            if (this.cursor)
                this.cursor.close();
        }
    };
    return Client;
}());
exports.default = Client;
//# sourceMappingURL=Client.js.map