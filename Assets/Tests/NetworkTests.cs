﻿using UnityEngine;
using UnityEngine.TestTools;
using NUnit.Framework;
using System.Collections;

public class NetworkTests {

    [UnityTest]
    public IEnumerator ConnectTest() {
        GameObject g = new GameObject();
        Networker network = g.AddComponent<Networker>();
        network.SocketMessage("authentication", "{ \"id\":\"Shura\", \"password\": \"\" } ");
        bool done = false;
        bool? result = null;
        network.On("auth-response", (string s) =>
        {
            done = true;
            result = bool.Parse(s);
        });
        while (!done)
        {
            yield return null;
        }
        Assert.NotNull(result);
        yield return null;
    }
}
