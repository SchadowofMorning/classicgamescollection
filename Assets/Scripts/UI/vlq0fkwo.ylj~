﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Newtonsoft.Json;

public class MainMenu : MonoBehaviour
{
    private Networker network;
    private GameObject loginPanel;
    private Transform lobbyPanel;
    private Transform lobbyList;
    private TMP_InputField nameField;
    private TMP_InputField passwordField;
    private string username;
    private string password;
    private User user;

    private List<Lobby> lobbys;

    //transfer variables from offload to main thread
    private bool clearTheList;
    public void Start()
    {
        PlayerPrefs.DeleteAll();
        network = GetComponent<Networker>();
        loginPanel = transform.Find("LoginPanel").gameObject;
        lobbyPanel = transform.Find("LobbyPanel");
        lobbyList = lobbyPanel.Find("Viewport").Find("Content");
        nameField = loginPanel.transform.Find("NameField").GetComponent<TMP_InputField>();
        passwordField = loginPanel.transform.Find("PasswordField").GetComponent<TMP_InputField>();
        network.OpenSocket();
        network.On("auth-res", HandleAuthRes);
        network.On("lobby-data", HandleLobbyData);
        lobbys = new List<Lobby>();
    }

    void Update()
    {
        if (clearTheList)
        {
            RefreshLobbyList();
            clearTheList = false;
        }
        if (PlayerPrefs.HasKey("User") && network.alive)
        {
            if (loginPanel.activeSelf) loginPanel.SetActive(false);
        } else
        {
            if (!PlayerPrefs.HasKey("User") && user != null)
            {
                PlayerPrefs.SetString("User", JsonUtility.ToJson(user));
            }
            if (!loginPanel.activeSelf) loginPanel.SetActive(true);
        }
    }
    
    private void RefreshLobbyList()
    {
        foreach (Transform t in lobbyList) Destroy(t.gameObject);
        foreach (Lobby l in lobbys) AddLobby(l);
    }
    private void AddLobby(Lobby lobby)
    {
        RectTransform prefab = Resources.Load<RectTransform>("Menu/Lobby");
        Transform instance = Instantiate(prefab);
        instance.SetParent(lobbyList, false);
        instance. = new Vector3(0, -60 - (lobbys.Count * 90), 0);
        TMP_Text title = instance.Find("Title").GetComponent<TMP_Text>();
        title.text = lobby.title;
    }
    /// <summary>
    /// Delegate that handle AuthRes
    /// </summary>
    /// <param name="data"></param>
    private void HandleAuthRes(string data)
    {
        bool result = bool.Parse(data); //you can parse anything as long it is an exact string
        if (result)
        {
            user = new User(username, password);
            network.SocketMessage("get-lobbys", "{}");
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="data"></param>
    private void HandleLobbyData(string data)
    {
        Lobby[] result = JsonConvert.DeserializeObject<Lobby[]>(data);
        lobbys.Clear();
        foreach (Lobby l in result) lobbys.Add(l);
        clearTheList = true;
    }
    /// <summary>
    /// Sends Auth data to server
    /// </summary>
    public void SendAuth()
    {
        Debug.Log("Sending Auth answer");
        string hash = String.Empty;
        if (!(passwordField.text == String.Empty))
        {
            byte[] values = Encoding.UTF8.GetBytes(passwordField.text);
            SHA256Managed sha = new SHA256Managed();
            foreach (byte b in sha.ComputeHash(values)) { hash += b.ToString("x2"); }
        }
        username = nameField.text;
        password = hash;
        network.SocketMessage("auth-data", string.Format("{{ \"id\":\"{0}\", \"password\":\"{1}\" }}", nameField.text, hash)); //data has to be transported in json
    }
}