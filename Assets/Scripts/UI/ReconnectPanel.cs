﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class ReconnectPanel : MonoBehaviour {
    [SerializeField]
    private MainMenu menu;
    [SerializeField]
    private Networker network;
    [SerializeField]
    private TMP_Text reconText;
    private bool runningCoroutine = false;
	// Update is called once per frame
	void Update () {
        if (network.alive) menu.state = MenuState.Login;
        else if(!runningCoroutine)
        {
            runningCoroutine = true;
            StartCoroutine(AutoReconnect());
        }
	}
    public void Reconnect()
    {
        network.OpenSocket();
    }
    IEnumerator AutoReconnect()
    {
        for (int i = 0; i < 30; i++)
        {
            reconText.text = string.Format("You're Disconnected, \n Autoconnect in {0}", 30 - i);
            yield return new WaitForSeconds(1f);
        }
        Reconnect();
        runningCoroutine = false;
    }
}
