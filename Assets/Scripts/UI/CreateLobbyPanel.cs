﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using TMPro;
public class CreateLobbyPanel : MonoBehaviour
{
    [SerializeField]
    private MainMenu menu;
    [SerializeField]
    private Networker network;
    public void OpenCreateLobby()
    {
        menu.state = MenuState.CreateLobby;
    }
    public void CloseCreateLobby()
    {
        menu.state = MenuState.Lobby;
    }
    public void CreateLobby()
    {
        TMP_InputField title = transform.Find("TitleInput").GetComponent<TMP_InputField>();
        TMP_Dropdown type = transform.Find("TypeSelect").GetComponent<TMP_Dropdown>();
        Lobby lobby = new Lobby(title.text, type.value);
        network.SocketMessage("create-lobby", JsonUtility.ToJson(lobby));
        CloseCreateLobby();
    }
}