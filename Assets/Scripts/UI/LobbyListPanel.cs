﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using UnityEngine.UI;
using TMPro;
public class LobbyListPanel : MonoBehaviour
{
    [SerializeField]
    private MainMenu menu;
    [SerializeField]
    private Networker network;
    [SerializeField]
    private Transform lobbyList;

    private bool clearTheList = false;
    private List<Lobby> lobbys;
    private List<LobbyEntry> lobbyEntrys;
    private void Awake()
    {
        network.On("lobby-data", HandleLobbyData);
        network.On("lobby-changes", ChangeLobbyData);
        lobbys = new List<Lobby>();
        lobbyEntrys = new List<LobbyEntry>();
    }
    public void Update()
    {
        if (clearTheList)
        {
            RefreshLobbyList();
            clearTheList = false;
        }
    }
    private void RefreshLobbyList()
    {
        lobbyEntrys.Clear();
        foreach (Transform t in lobbyList) Destroy(t.gameObject);
        for (int i = 0; i < lobbys.Count; i++)
        {
            lobbyEntrys.Add(AddLobby(lobbys[i], i));
        }
    }
    public void Refresh()
    {
        lobbys = new List<Lobby>();
        GetLobbys();
    }
    public void GetLobbys()
    {
        network.SocketMessage("get-lobbys", "{}");
    }
    public void LeaveLobby()
    {
        network.SocketMessage("leave-lobby", "{}");
    }
    /// <summary>
    /// Adds Lobby to lobbyList, only use with RefreshLobbyList
    /// </summary>
    /// <param name="lobby"></param>
    private LobbyEntry AddLobby(Lobby lobby, int index)
    {
        LobbyEntry instance = LobbyEntry.CreateInstance(menu, lobbyList, index, lobby);
        RectTransform ll = lobbyList.GetComponent<RectTransform>();
        ll.sizeDelta = new Vector2(ll.sizeDelta.x, index * 90);
        return instance;
    }
    /// <summary>
    /// Adds Lobby received from server
    /// </summary>
    /// <param name="data"></param>

    private void HandleLobbyData(string data)
    {
        Lobby[] result = JsonConvert.DeserializeObject<Lobby[]>(data);
        Debug.Log(string.Format("Deserialized: {0}", result));
        lobbys = new List<Lobby>(result);
        clearTheList = true;
    }
    private void ChangeLobbyData(string data)
    {
        try
        {
            JObject result = JsonConvert.DeserializeObject<JObject>(data);
            Lobby l = JsonConvert.DeserializeObject<Lobby>(result["new_val"].ToString());
            Debug.Log(result["old_val"].ToString());
            if(result["old_val"].ToString() == string.Empty)
            {
                lobbys.Add(l);
                clearTheList = true;
            } else
            {
                IEnumerable<LobbyEntry> filtered = lobbyEntrys.Where(x => {
                    Debug.LogFormat("lobbyID: {0} newLobbyID: {1}", x.ID, l.id);
                    return x.ID == l.id; });
                    if(filtered.Count() > 0)
                        filtered.First().Lobby = l;
            }
        } catch(Exception e)
        {
            Debug.LogError(e.Message);
        }
    }       
    public void OpenCreateLobby()
    {
        menu.state = MenuState.CreateLobby;
    }
    public void CloseCreateLobby()
    {
        menu.state = MenuState.Lobby;
    }
}