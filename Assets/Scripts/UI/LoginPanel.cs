﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;
using System.Security.Cryptography;
using TMPro;
using System.Text.RegularExpressions;

public class LoginPanel : MonoBehaviour
{
    [SerializeField]
    private MainMenu mainMenu;
    [SerializeField] 
    private Networker network;
    [SerializeField]
    private TMP_InputField idField;
    [SerializeField]
    private TMP_InputField passwordField;
    private string username;
    private string password;
    private void Awake()
    {
        network.On("auth-response", HandleAuthRes);
    }
    public void Active(bool active)
    {
        if (gameObject.activeSelf != active) gameObject.SetActive(true);
    }
    public void SendAuth()
    {
        string hash = String.Empty;
        if (!(passwordField.text == String.Empty))
        {
            byte[] values = Encoding.UTF8.GetBytes(passwordField.text);
            SHA256Managed sha = new SHA256Managed();
            foreach (byte b in sha.ComputeHash(values)) { hash += b.ToString("x2"); }
        }
        string username = Regex.Replace(idField.text, "[^a-zA-Z0-9_.]+", "", RegexOptions.Compiled);
        network.SocketMessage("authentication", string.Format("{{ \"id\": \"{0}\", \"password\": \"{1}\" }}", username, hash));
    }
    public void HandleAuthRes(string data)
    {
        bool result = bool.Parse(data); //you can parse anything as long it is an exact string
        if (result)
        {
            User user = new User(idField.text, passwordField.text);
            Debug.Log(user);
            network.SocketMessage("get-lobbys", "{}");
            mainMenu.SetUser(user);
        }
    }
}