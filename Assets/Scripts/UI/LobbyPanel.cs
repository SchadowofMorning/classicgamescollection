﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using TMPro;
public class LobbyPanel : MonoBehaviour
{
    [SerializeField]
    private MainMenu menu;
    [SerializeField]
    private Networker network;
    [HideInInspector]
    public Lobby currentLobby;
    private Lobby lobbyUpdate;
    public int lobbyType { get { return currentLobby.type; } }
    
    private void Start()
    {
        network.On("lobby-update", HandleUpdateLobby);
        network.On("leave-lobby", Leave);
    }
    public void UpdateLobbyPanel()
    {
        currentLobby = lobbyUpdate;
        if(currentLobby == null) return;
        TMP_Text pList = transform.Find("PlayerList").GetComponent<TMP_Text>();
        pList.text = String.Empty;
        if (currentLobby.users == null) currentLobby.users = new List<string>();
        foreach (string s in currentLobby.users) pList.text += s + "\n";
    }
    void Update()
    {
        if (lobbyUpdate != currentLobby) UpdateLobbyPanel();
        if (currentLobby != null)
        {
            menu.state = MenuState.Lobby;
            if (currentLobby.running && menu.game == null) menu.JoinGame(currentLobby.id);
        }
    }
    private void HandleUpdateLobby(string data)
    {
        lobbyUpdate = JsonConvert.DeserializeObject<Lobby>(data);
        menu.state = MenuState.Lobby;
    }
    public void StartGame()
    {
        network.SocketMessage("start-lobby", '"' + currentLobby.id + '"');
    }
    
    public void LeaveLobby()
    {
        network.SocketMessage("leave-lobby", "{}");
    }
    public void Leave(string s)
    {
        menu.state = MenuState.List;
        menu.RefreshLobbys();
        currentLobby = null;
    }

}