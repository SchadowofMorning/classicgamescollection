﻿using System;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Chess;
using GO;

public class MainMenu : MonoBehaviour
{
    private Networker network;
    [SerializeField]
    private LoginPanel loginPanel;
    [SerializeField]
    private LobbyListPanel lobbyListPanel;
    [SerializeField]
    private LobbyPanel lobbyPanel;
    [SerializeField]
    private CreateLobbyPanel createLobbyPanel;
    [SerializeField]
    private ReconnectPanel reconnectPanel;
    private User user;
    private int? startGame;
    public IGame game;
    public MenuState state = MenuState.Login;
    [SerializeField]
    private new Camera camera;
    public bool lobbyHost { get { return user.id == lobbyPanel.currentLobby.users[0]; } }
    public void Start()
    {
        PlayerPrefs.DeleteAll();
        network = GetComponent<Networker>();
        network.OpenSocket();
        network.On("created-lobby", Join);
        network.On("join-game", JoinGame);
    }

    void Update()
    {
        switch (state)
        {
            case MenuState.Login:
                SetState(loginPanel, true);
                SetState(lobbyListPanel, false);
                SetState(createLobbyPanel, false);
                SetState(lobbyPanel, false);
                SetState(reconnectPanel, false);
                break;
            case MenuState.List:
                SetState(loginPanel, false);
                SetState(lobbyListPanel, true);
                SetState(createLobbyPanel, false);
                SetState(lobbyPanel, false);
                SetState(reconnectPanel, false);
                break;
            case MenuState.CreateLobby:
                SetState(loginPanel, false);
                SetState(lobbyListPanel, true);
                SetState(createLobbyPanel, true);
                SetState(lobbyPanel, false);
                SetState(reconnectPanel, false);
                break;
            case MenuState.Lobby:
                SetState(loginPanel, false);
                SetState(lobbyListPanel, false);
                SetState(createLobbyPanel, false);
                SetState(lobbyPanel, true);
                SetState(reconnectPanel, false);
                break;
            case MenuState.Disconnected:
                SetState(loginPanel, false);
                SetState(lobbyListPanel, false);
                SetState(createLobbyPanel, false);
                SetState(lobbyPanel, false);
                SetState(reconnectPanel, true);
                break;
            case MenuState.Game:
                SetState(loginPanel, false);
                SetState(lobbyListPanel, false);
                SetState(createLobbyPanel, false);
                SetState(lobbyPanel, false);
                SetState(reconnectPanel, false);
                break;
        }
        if (startGame.HasValue)
        {
            StartGame(startGame.Value);
            Debug.Log(string.Format("Start Game: {0}", startGame));
            startGame = null;
        }
        if (!network.alive) state = MenuState.Disconnected;
    }
    public void SetUser(User _user)
    {
        user = _user;
        state = MenuState.List;
    }
    private void SetState<T>(T t, bool active) where T : MonoBehaviour
    {
        if (t.gameObject.activeSelf != active) t.gameObject.SetActive(active);
    }
    private void SetState(Transform t, bool active)
    {
        if (t.gameObject.activeSelf != active) t.gameObject.SetActive(active);
    }

    public void Join(string id)
    {
        network.SocketMessage("join-lobby", '"' + id + '"');
    }
    public void JoinGame(string id)
    {
        network.SocketMessage("join-game", '"' + id + '"');
        startGame = lobbyPanel.lobbyType;
    }
    public void StartGame(int val)
    {
        camera.gameObject.SetActive(false);
        
        switch (val)
        {
            case 0:
                game = Instantiate(Resources.Load<Logic>("Go/Logic"));
                Logic l = (Logic)game;
                l.name = "Logic";
                game.StartGame(lobbyHost ? 1 : 0, network);
                break;
            case 1:
                game = Instantiate(Resources.Load<Controller>("Chess/ChessGame"));
                game.StartGame(lobbyHost, network);
                break;
        }
        Debug.Log(lobbyPanel.currentLobby.users[0]);
        Debug.Log(lobbyHost);
        state = MenuState.Game;
        network.On("game-update", game.UpdateState);
        gameObject.SetActive(false);
    }
    public void RefreshLobbys()
    {
        lobbyListPanel.Refresh();
    }
}
public enum MenuState
{
    Login,
    Disconnected,
    List,
    CreateLobby,
    Lobby,
    Game
}