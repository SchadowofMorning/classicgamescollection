﻿using System;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
[Serializable]
public class Lobby
{
    public string id;
    public string title;
    public int type;
    public List<string> users;
    public bool running;
    public int maxSize;

    public Lobby()
    {
        
    }
    public Lobby(string _title, int _type)
    {
        title = _title;
        type = _type;
        users = new List<string>();
        maxSize = 2;
    }
    public override string ToString()
    {
        return JsonUtility.ToJson(this);
    }
}