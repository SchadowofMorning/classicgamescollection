﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
public class LobbyEntry : MonoBehaviour
{
    [SerializeField]
    private TMP_Text title;
    [SerializeField]
    private TMP_Text playerCounter;
    [SerializeField]
    private Button button;
    private Lobby lobby;
    public string ID
    {
        get
        {
            return lobby.id;
        }
    }
    public Lobby Lobby
    {
        get
        {
            return lobby;
        }
        set
        {
            lobby = value;
            title.text = value.title;
            playerCounter.text = string.Format("Players: {0}/{1}", value.users.Count, value.maxSize);
        }
    }
    public string Title
    {
        get
        {
            return title.text;
        }

        set
        {
            lobby.title = value;
            title.text = value;
        }
    }
    public List<string> Users
    {
        get
        {
            return lobby.users;
        }
        set
        {
            lobby.users = value;
            playerCounter.text = string.Format("Players: {0}/{1}", lobby.users.Count, lobby.maxSize);
        }
    }
    public static LobbyEntry CreateInstance(MainMenu menu, Transform lobbyList, int index, Lobby lobby)
    {
        LobbyEntry prefab = Resources.Load<LobbyEntry>("Menu/Lobby");
        LobbyEntry instance = Instantiate(prefab);
        RectTransform rt = instance.GetComponent<RectTransform>();
        rt.SetParent(lobbyList, false);
        rt.anchoredPosition = new Vector3(0, -60 - (index * 90), 0);
        instance.Lobby = lobby;
        instance.button.onClick.AddListener(() => { menu.Join(lobby.id); });
        return instance;

    }
}
