﻿using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Chess
{
    public class Player : MonoBehaviour
    {

        public Tile selectedTile;
        public Piece selectedPiece;
        public bool isWhite;

        public void HasTurn()
        {
            if (Input.GetMouseButtonDown(0))
            {
                RaycastHit hit;
                Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit);
                Collider col = hit.collider;
                if (col != null)
                {
                    Tile highlightedTile = col.GetComponent<Tile>();
                    if (highlightedTile != null)
                    {
                        Piece p = highlightedTile.GetPiece();
                        if (p != null)
                        {
                            if (selectedPiece == null)
                            {
                                if (p.isWhite == this.isWhite)
                                {
                                    selectedTile = highlightedTile;
                                    selectedPiece = p;
                                    Controller.instance.SelectTile(highlightedTile);
                                }
                            }
                            else
                            {
                                if (p.isWhite != selectedPiece.isWhite)
                                {
                                    if (selectedPiece.legalMoves.Contains(highlightedTile) && !(p is King))
                                        Controller.instance.Move(new System.Tuple<Coordinate, Coordinate>(selectedPiece.coordinate, highlightedTile.coordinate), false);
                                }
                                else
                                    Deselect();
                            }

                        }
                        else
                        {
                            if (selectedPiece != null)
                            {
                                if (selectedPiece.legalMoves.Contains(highlightedTile))
                                    Controller.instance.Move(new System.Tuple<Coordinate, Coordinate>(selectedPiece.coordinate, highlightedTile.coordinate), false);
                            }
                        }
                    }
                    else Deselect();
                }
                else Deselect();
            }
        }
        public void Deselect()
        {
            if (selectedTile != null)
            {
                selectedTile.ChangeColor(0);
                selectedTile = null;
            }
            if (selectedPiece != null)
            {
                foreach (Tile ti in ChessBoard.instance.tiles)
                {
                    ti.ChangeColor(0);
                }
                selectedPiece = null;
            }
        }

    }
}
