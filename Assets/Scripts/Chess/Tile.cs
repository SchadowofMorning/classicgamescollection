﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Chess
{
    public class Tile : MonoBehaviour
    {
        public Coordinate coordinate;
        public Piece currentPiece;
        private Renderer rend
        {
            get
            {
                return this.GetComponent<Renderer>();
            }
        }
        private Material[] colors;
        public bool[] contests = new bool[2];
        public void InitializeTile(Coordinate c, bool iswhite)
        {
            coordinate = c;
            this.transform.position = c.vector;
            string normalColorPath = "Chess/Materials/";
            if (iswhite)
                normalColorPath += "White";
            else normalColorPath += "Black";
            colors = new Material[]
            {
            Resources.Load<Material>(normalColorPath),
            Resources.Load<Material>("Chess/Materials/Selected"),
            Resources.Load<Material>("Chess/Materials/Selectable"),
            Resources.Load<Material>("Chess/Materials/Enemy"),
            Resources.Load<Material>("Chess/Materials/Check")
            };
            ChangeColor(0);
            currentPiece = null;
        }
        public void ChangePiece(Piece p) => currentPiece = p;

        public Piece GetPiece()
        {
            return this.currentPiece;
        }

        public void ChangeColor(int index)
        {
            if (index > colors.Length - 1 || index < 0)
                throw new System.IndexOutOfRangeException("Index of wanted material out of bounds");
            else
                rend.material = colors[index];
        }

    }
}
