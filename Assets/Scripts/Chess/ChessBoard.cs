﻿using System;
using Newtonsoft.Json;
using System.Collections;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;

namespace Chess
{
    public class ChessBoard : MonoBehaviour
    {

        public Tile[,] tiles = new Tile[8, 8];
        public List<Piece> pieces;
        public Piece[] whitePieces
        {
            get
            {
                return pieces.Where(x => x.isWhite).ToArray();
            }
        }
        public Piece[] blackPieces
        {
            get
            {
                return pieces.Where(x => !x.isWhite).ToArray();
            }
        }


        public Tile this[Coordinate c]
        {
            get
            {
                return GetTileFromCoord(c);
            }
        }
        #region Sigleton
        public static ChessBoard instance;
        private void Awake()
        {
            if (instance != null && FindObjectOfType<ChessBoard>() != this)
                Destroy(this);
            else instance = this;
        }
        #endregion

        public void InitBoard()
        {
            for (int x = 0; x <= tiles.GetUpperBound(0); x++)
            {
                for (int y = 0; y <= tiles.GetUpperBound(1); y++)
                {
                    bool white;
                    if ((x + y) % 2 == 0)
                        white = false;
                    else white = true;
                    tiles[x, y] = Instantiate(Resources.Load<Tile>("Chess/Tile"));
                    tiles[x, y].InitializeTile(new Coordinate(x, y), white);
                    tiles[x, y].name = "Tile: " + tiles[x, y].coordinate.ToString();
                }
            }
        }

        public void InitPieces()
        {
            pieces = new List<Piece>();
            for (int y = 0; y <= tiles.GetUpperBound(1); y++)
            {
                for (int x = 0; x <= tiles.GetUpperBound(0); x++)
                {
                    Piece p = null;
                    bool white;
                    string path = "";
                    if (y == 0 || y == 1)
                        white = true;
                    else white = false;
                    if (y == 0 || y == 7)
                    {
                        if (x == 0 || x == 7)
                            path = "Tower";
                        else if (x == 1 || x == 6)
                            path = "Knight";
                        else if (x == 2 || x == 5)
                            path = "Bishop";
                        else if (x == 3)
                            path = "Queen";
                        else path = "King";
                    }
                    else if (y == 1 || y == 6)
                        path = "Pawn";
                    p = Resources.Load<Piece>("Chess/Pieces/" + path);
                    if (p != null)
                    {
                        Piece instanced = Instantiate(p);
                        instanced.InitalizePiece(new Coordinate(x, y), white);
                        tiles[x, y].ChangePiece(instanced);
                        pieces.Add(instanced);
                    }
                }
            }
        }
        public Coordinate[] CheckCastle(bool isWhite)
        {
            List<Coordinate> castles = new List<Coordinate>();
            Tile kingTile;
            if (isWhite)
                kingTile = this[new Coordinate(4, 0)];
            else
                kingTile = this[new Coordinate(4, 7)];
            if (kingTile.currentPiece != null)
            {
                if (kingTile.currentPiece is King)
                {
                    if (!kingTile.currentPiece.tookFirstTurn)
                    {
                        //+10 for Kevindor
                        for (int i = 1; i < 5; i++)
                        {
                            if (this[kingTile.coordinate - new Coordinate(i, 0)].currentPiece != null)
                            {
                                if ((this[kingTile.coordinate - new Coordinate(i, 0)].currentPiece is Tower))
                                {
                                    if (!this[kingTile.coordinate - new Coordinate(i, 0)].currentPiece.tookFirstTurn)
                                        castles.Add(new Coordinate(-2, 0));
                                }
                                else
                                    break;
                            }
                        }
                        for (int i = 1; i < 4; i++)
                        {
                            if (this[kingTile.coordinate + new Coordinate(i, 0)].currentPiece != null)
                            {
                                if ((this[kingTile.coordinate + new Coordinate(i, 0)].currentPiece is Tower))
                                {
                                    if (!this[kingTile.coordinate - new Coordinate(i, 0)].currentPiece.tookFirstTurn)
                                        castles.Add(new Coordinate(2, 0));
                                }
                                else
                                    break;
                            }
                        }
                    }
                }
            }
            return castles.ToArray();
        }

        public bool InBoard(Coordinate c)
        {
            return ((c.x >= tiles.GetLowerBound(0) && c.x <= tiles.GetUpperBound(0)) && (c.y >= tiles.GetLowerBound(1) && c.y <= tiles.GetUpperBound(1)));
        }
        public Piece GetPieceFromCoord(Coordinate c)
        {
            if (InBoard(c))
                return tiles[c.x, c.y].currentPiece;
            else return null;
        }
        public Tile GetTileFromCoord(Coordinate c)
        {
            if (InBoard(c))
                return tiles[c.x, c.y];
            else return null;
        }
        public T[] GetPieceByTypeAndColor<T>(bool color) where T : Piece
        {
            var query = tiles.Cast<Tile>();
            IEnumerable<Tile> result = query.Where(x => { return x.currentPiece != null && x.currentPiece.GetType() == typeof(T) && x.currentPiece.isWhite == color; });
            List<T> pieces = new List<T>();
            foreach (Tile tile in result) pieces.Add((T)tile.currentPiece);
            return pieces.ToArray();
        }

    }
}