﻿using System.Linq;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Chess
{
    public class Controller : MonoBehaviour, IGame
    {
        #region Singleton
        public static Controller instance;
        private void Awake()
        {
            Controller other = FindObjectOfType<Controller>();
            if (other != null && other != this)
                Destroy(this);
            else instance = this;
        }

        #endregion

        public event Action turnEnd;
        public bool whitehasTurn;
        public Player current;
        public bool isWhite;
        public List<Tuple<Coordinate, Coordinate>> moves;
        public Networker network;
        private TurnManager turnManager;
        private bool recievedNewUpdate;
        public Tuple<Coordinate, bool>[] checkMovesResult;
        private void StartChessGame()
        {
            ChessBoard.instance.InitBoard();
            ChessBoard.instance.InitPieces();
            turnManager = new TurnManager();
            current = this.GetComponentInChildren<Player>();
            current.isWhite = this.isWhite;
            foreach (Piece p in ChessBoard.instance.pieces)
            {
                turnEnd += p.CheckMoves;
            }

        }

        private void Update()
        {
            if (recievedNewUpdate)
                ProcessServerUpdate();
            if (whitehasTurn == current.isWhite)
                current.HasTurn();
            if (!ChessBoard.instance.GetPieceByTypeAndColor<Piece>(true).Any(x => x.Legal.Length > 0))
                GameWon(false);
            if (!ChessBoard.instance.GetPieceByTypeAndColor<Piece>(false).Any(x => x.Legal.Length > 0))
                GameWon(true);

        }
        public void GameWon(bool whiteWon)
        {
            if (whiteWon)
                Debug.Log("White Won the game");
            else
                Debug.Log("Black won the game");
        }

        public void StalinTurn(bool theoretical)
        {
            if (!theoretical)
                whitehasTurn = !whitehasTurn;
            foreach (Tile t in ChessBoard.instance.tiles)
            {
                t.contests[0] = false;
                t.contests[1] = false;
                t.ChangeColor(0);
                if (t.currentPiece is King)
                {
                    t.currentPiece.GetComponent<King>().inChess = false;
                    t.currentPiece.GetComponent<King>().attackTiles = new List<Tile>();
                }
            }
            turnEnd();
        }
        public void SelectTile(Tile t)
        {
            t.currentPiece.legalMoves = new List<Tile>();
            t.currentPiece.legalMoves.AddRange(t.currentPiece.Moves.Where(x => !CheckTurn(new Tuple<Coordinate, Coordinate>(t.coordinate, x.coordinate), whitehasTurn).Item2));
            t.ChangeColor(1);
            foreach (Tile ti in current.selectedPiece.legalMoves)
            {
                if (ti.currentPiece == null || ti.currentPiece.coordinate != ti.coordinate)
                {
                    if (ti.currentPiece != null)
                    {
                        if (current.selectedPiece is Pawn && ti.currentPiece.coordinate != ti.coordinate && current.selectedPiece.isWhite != ti.currentPiece.isWhite)
                            ti.ChangeColor(3);
                        else
                            ti.ChangeColor(2);
                    }
                    else ti.ChangeColor(2);
                }
                else if (ti.currentPiece.isWhite != current.selectedPiece.isWhite)
                {
                    if (ti.currentPiece is King)
                        ti.ChangeColor(4);
                    else
                        ti.ChangeColor(3);
                }
            }
        }
        public void HitPiece(Piece attacker, Tile t, bool theoretical)
        {
            if (attacker == null) return;
            Coordinate attackCoord = attacker.coordinate;
            Piece victim = t.currentPiece;
            victim.Move(new Coordinate(8, 9), theoretical);
            ChessBoard.instance[attacker.coordinate].currentPiece = null;
            attacker.Move(t.coordinate, theoretical);
            t.currentPiece = attacker;
            turnManager.AddTurn(new Turn(new Tuple<Coordinate, Coordinate>(attackCoord, attacker.coordinate), attacker.isWhite, victim), theoretical);
            StalinTurn(theoretical);
            if (!theoretical)
            {
                current.Deselect();
            }
        }

        public void MovePiece(Piece p, Tile t, bool theoretical)
        {
            ChessBoard.instance.GetTileFromCoord(p.coordinate).currentPiece = null;
            bool castled = false;
            Tile castleTowerTile = null;
            Tile towerCastleTile = null;
            if (p is King)
            {
                if (t.coordinate == p.coordinate - new Coordinate(2, 0) || t.coordinate == p.coordinate + new Coordinate(2, 0))
                {
                    castled = true;
                    if (p.coordinate.x + 2 == t.coordinate.x)
                    {
                        castleTowerTile = ChessBoard.instance[(t.coordinate + new Coordinate(1, 0))];
                        towerCastleTile = ChessBoard.instance[(t.coordinate - new Coordinate(1, 0))];
                    }
                    else if (p.coordinate.x - 2 == t.coordinate.x)
                    {
                        castleTowerTile = ChessBoard.instance[(t.coordinate - new Coordinate(2, 0))];
                        towerCastleTile = ChessBoard.instance[(t.coordinate + new Coordinate(1, 0))];
                    }
                    castleTowerTile.currentPiece.Move(towerCastleTile.coordinate, theoretical);
                    towerCastleTile.currentPiece = castleTowerTile.currentPiece;
                    castleTowerTile.currentPiece = null;
                }

            }
            Coordinate pieceCoord = p.coordinate;
            t.currentPiece = p;
            p.Move(t.coordinate, theoretical);
            if (!castled)
                turnManager.AddTurn(new Turn(new Tuple<Coordinate, Coordinate>(pieceCoord, t.coordinate), p.isWhite, null), theoretical);
            else turnManager.AddTurn(new Turn(new Tuple<Coordinate, Coordinate>(pieceCoord, t.coordinate), new Tuple<Coordinate, Coordinate>(castleTowerTile.coordinate, towerCastleTile.coordinate), p.isWhite, null), theoretical);
            StalinTurn(theoretical);
            if (!theoretical)
            {
                current.Deselect();
            }
        }
        public Tuple<Coordinate, bool> CheckTurn(Tuple<Coordinate, Coordinate> input, bool isWhite)
        {
            Move(input, true);
            Tuple<Coordinate, bool> result = new Tuple<Coordinate, bool>(input.Item2, ChessBoard.instance.GetPieceByTypeAndColor<King>(isWhite).First().inChess);
            turnManager.RevertLastTurn();
            return result;
        }
        public void Move(Tuple<Coordinate, Coordinate> move, bool theoretical)
        {
            Piece p = ChessBoard.instance[move.Item1].currentPiece;
            Tile t = ChessBoard.instance[move.Item2];
            if (p is Pawn)
            {
                if (move.Item1.x != move.Item2.x)
                {
                    if (t.currentPiece.coordinate != t.coordinate)
                    {
                        Tile tile = ChessBoard.instance.GetTileFromCoord(t.coordinate + (p.isWhite ? Coordinate.South : Coordinate.North));
                        tile.currentPiece = null;
                    }
                    HitPiece(p, t, theoretical);
                }
                else
                {
                    MovePiece(p, t, theoretical);
                }
            }
            else
            {
                if (t.currentPiece != null)
                {
                    HitPiece(p, t, theoretical);
                }
                else
                {
                    MovePiece(p, t, theoretical);
                }
            }
        }


        public void UpdateState(string data)
        {
            JObject obj = JsonConvert.DeserializeObject<JObject>(data);
            JToken jt = obj["new_val"]["turns"];
            Debug.Log("Recieved " + jt.ToString());
            Turn[] turns = JsonConvert.DeserializeObject<Turn[]>(jt.ToString());
            turnManager.turns = new Queue<Turn>(turns);
            recievedNewUpdate = true;
        }
        private void ProcessServerUpdate()
        {
            recievedNewUpdate = false;
            Turn lastTurn = turnManager.GetLastTurn();
            turnManager.turns.ToList().Remove(lastTurn);
            Move(lastTurn.move, false);
            
        }

        public void StartGame(object o, Networker net)
        {
            isWhite = (bool)o;
            whitehasTurn = true;
            network = net;
            StartChessGame();
        }
    }


    [Serializable]
    public struct Turn
    {
        public Tuple<Coordinate, Coordinate> move;
        public Tuple<Coordinate, Coordinate> move2;
        public bool madeByWhite;
        [JsonIgnore]
        public Piece hasBeenHit;

        public Turn(Tuple<Coordinate, Coordinate> fromTo, bool wasWhite, Piece gotHit)
        {
            move = fromTo;
            move2 = null;
            madeByWhite = wasWhite;
            hasBeenHit = gotHit;
        }
        public Turn(Tuple<Coordinate, Coordinate> move1, Tuple<Coordinate, Coordinate> move_2, bool wasWhite, Piece gotHit)
        {
            move = move1;
            move2 = move_2;
            madeByWhite = wasWhite;
            hasBeenHit = gotHit;
        }

        public override string ToString()
        {
            return string.Format("{0} moved a piece from {1} to {2}", madeByWhite, move.Item1, move.Item2);
        }
    }

    public class TurnManager
    {
        public Queue<Turn> turns;

        public TurnManager()
        {
            turns = new Queue<Turn>();
        }
        public void AddTurn(Turn t, bool theoretical)
        {
            turns.Enqueue(t);
            Debug.Log(JsonConvert.SerializeObject(turns.ToArray()));
            if (!theoretical)
                Controller.instance.network.SocketMessage("game-update", JsonConvert.SerializeObject(turns.ToArray()));
        }
        public Turn GetLastTurn()
        {
            return turns.Last();
        }
        //+10 for Kevindor
        public void RevertLastTurn()
        {
            Turn lastTurn = GetLastTurn();
            Piece movedPiece = ChessBoard.instance.GetPieceFromCoord(lastTurn.move.Item2);
            movedPiece.Move(lastTurn.move.Item1, true);
            ChessBoard.instance[lastTurn.move.Item1].currentPiece = movedPiece;
            if (lastTurn.hasBeenHit != null)
            {
                ChessBoard.instance[lastTurn.move.Item2].currentPiece = lastTurn.hasBeenHit;
                lastTurn.hasBeenHit.Move(lastTurn.move.Item2, true);
            }
            else ChessBoard.instance[lastTurn.move.Item2].currentPiece = null;
            if (lastTurn.move2 != null)
            {
                Piece piece = ChessBoard.instance.GetPieceFromCoord(lastTurn.move2.Item2);
                piece.Move(lastTurn.move2.Item1, true);
                ChessBoard.instance[lastTurn.move2.Item2].currentPiece = null;
                ChessBoard.instance[lastTurn.move2.Item1].currentPiece = piece;
            }
            turns.ToList().Remove(lastTurn);
            turns.TrimExcess();
            Controller.instance.StalinTurn(true);
        }

    }

}