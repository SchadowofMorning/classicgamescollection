﻿using System.Collections;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace Chess
{
    [System.Serializable]
    public abstract class Piece : MonoBehaviour
    {
        public bool tookFirstTurn;
        public bool isWhite;
        public Coordinate coordinate;
        private string standardName;
        public abstract int repetitions { get; }
        public abstract Coordinate[] MovePattern { get; }
        public Tile[] Moves
        {
            get
            {
                List<Tile> tiles = new List<Tile>();
                foreach (Coordinate c in MovePattern)
                {
                    int reps;
                    if (this is Pawn && ((c == new Coordinate(1, 1)) || (c == new Coordinate(-1, 1)) || c == new Coordinate(1, -1) || c == new Coordinate(-1, -1)))
                        reps = 1;
                    else reps = repetitions;
                    for (int i = 1; i <= reps; i++)
                    {
                        if (ChessBoard.instance.InBoard(this.coordinate + c * i))
                        {
                            Tile t = ChessBoard.instance.tiles[this.coordinate.x + c.x * i, this.coordinate.y + c.y * i];
                            if (this is Pawn)
                            {
                                if ((c == Coordinate.North || c == Coordinate.South) && t.currentPiece != null)
                                    break;
                                else if (c == new Coordinate(1, 1) || c == new Coordinate(-1, 1) || c == new Coordinate(1, -1) || c == new Coordinate(-1, -1))
                                {
                                    Contest(t);
                                    if (t.currentPiece == null)
                                        break;
                                    else
                                    {
                                        if (t.currentPiece.isWhite != this.isWhite)
                                        {
                                            tiles.Add(t);
                                            if (t.currentPiece is King)
                                                t.currentPiece.GetComponent<King>().inChess = true;
                                        }
                                    }
                                }
                                else tiles.Add(t);
                            }
                            else if (t.currentPiece == null || t.currentPiece.isWhite != this.isWhite)
                            {
                                Contest(t);
                                tiles.Add(t);
                                if (t.currentPiece != null)
                                {
                                    if (t.coordinate == t.currentPiece.coordinate)
                                    {
                                        if (t.currentPiece is King)
                                            t.currentPiece.GetComponent<King>().inChess = true;
                                        break;
                                    }
                                }
                            }
                            else
                            {
                                Contest(t);
                                if (t.currentPiece.coordinate != t.coordinate)
                                    tiles.Add(t);
                                else break;
                            }
                        }
                    }
                }
                return tiles.ToArray();
            }
        }
        public List<Tile> legalMoves;
        public Tile[] Legal
        {
            get
            {
                return Moves.Where(x => !Controller.instance.CheckTurn(new Tuple<Coordinate, Coordinate>(coordinate, x.coordinate), Controller.instance.whitehasTurn).Item2).ToArray();
            }
        }
        public virtual void InitalizePiece(Coordinate coord, bool white)
        {
            coordinate = coord;
            standardName = name.Replace("(Clone)", "");
            this.transform.position = new Vector3(coordinate.x, 1, coordinate.y);
            isWhite = white;
            string nameIsWhite;
            Renderer rend = this.GetComponent<Renderer>();
            if (isWhite)
            {
                rend.material = Resources.Load<Material>("Chess/Materials/PieceWhite");
                nameIsWhite = "White";
            }
            else
            {
                rend.material = Resources.Load<Material>("Chess/Materials/PieceBlack");
                nameIsWhite = "Black";
            }
            standardName = string.Format("{0} {1}", nameIsWhite, standardName);
            name = string.Format("{0} at {1}", standardName, coordinate);
            Controller.instance.turnEnd += CheckMoves;
        }
        public void Contest(Tile t)
        {
            if (isWhite)
                t.contests[0] = true;
            else t.contests[1] = true;
        }
        public virtual void CheckMoves()
        {
            Moves.ToString();
        }
        public virtual void Move(Coordinate c, bool theoretical)
        {
            if (!theoretical)
                if (!tookFirstTurn)
                    tookFirstTurn = true;
            this.coordinate = c;
            this.name = string.Format("{0} at {1}", standardName, coordinate);
            this.transform.position = c.vector + new Vector3(0, 1, 0);
        }
        public void CheckForLegalMoves()
        {
            legalMoves = new List<Tile>();
            legalMoves.AddRange(Moves.Where(x => !Controller.instance.CheckTurn(new Tuple<Coordinate, Coordinate>(coordinate, x.coordinate), Controller.instance.whitehasTurn).Item2));
        }


    }
}