﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Chess
{
    public class Queen : Piece
    {
        public override int repetitions
        {
            get
            {
                return 8;
            }
        }
        public override Coordinate[] MovePattern
        {
            get
            {
                return new Coordinate[]
                {
                Coordinate.East,
                Coordinate.South,
                Coordinate.West,
                Coordinate.North,
                Coordinate.East + Coordinate.North,
                Coordinate.East + Coordinate.South,
                Coordinate.West + Coordinate.South,
                Coordinate.West + Coordinate.North
                };
            }
        }
    }
}
