﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Chess
{
    public class Tower : Piece
    {
        public override int repetitions
        {
            get
            {
                return 8;
            }
        }
        public override Coordinate[] MovePattern
        {
            get
            {
                return Coordinate.StandardDistance;
            }
        }
    }
}