﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Chess
{
    public class King : Piece
    {
        public bool inChess;
        public List<Tile> attackTiles;
        public override int repetitions
        {
            get
            {
                return 1;
            }
        }
        public override Coordinate[] MovePattern
        {
            get
            {
                Coordinate[] dirs = new Coordinate[]
                {
                Coordinate.East,
                Coordinate.South,
                Coordinate.West,
                Coordinate.North,
                Coordinate.East + Coordinate.North,
                Coordinate.East + Coordinate.South,
                Coordinate.West + Coordinate.South,
                Coordinate.West + Coordinate.North
                };
                List<Coordinate> moves = new List<Coordinate>();
                foreach (Coordinate c in dirs)
                {
                    if (ChessBoard.instance.InBoard(this.coordinate + c))
                    {
                        Tile t = ChessBoard.instance.GetTileFromCoord(this.coordinate + c);
                        if (!t.contests[(this.isWhite ? 1 : 0)])
                            moves.Add(c);
                    }
                }
                if (!tookFirstTurn)
                    moves.AddRange(ChessBoard.instance.CheckCastle(this.isWhite));
                return moves.ToArray();
            }
        }
        public override void InitalizePiece(Coordinate coord, bool white)
        {
            base.InitalizePiece(coord, white);
            attackTiles = new List<Tile>();
        }

        public void AddAttacks(Tile[] attackPath)
        {
            attackTiles.AddRange(attackPath);
        }

        private void Update()
        {
            if (this.inChess)
                ChessBoard.instance[coordinate].ChangeColor(4);
            
        }
    }
}
