﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Chess
{
    public class Pawn : Piece
    {

        public Tile enPassed;
        public int promotion_Y;
        public override int repetitions
        {
            get
            {
                if (tookFirstTurn)
                    return 1;
                else return 2;
            }
        }
        public override Coordinate[] MovePattern
        {
            get
            {
                List<Coordinate> moves = new List<Coordinate>();
                int up;
                if (isWhite)
                {
                    moves.Add(Coordinate.North);
                    up = 1;
                    promotion_Y = 7;
                }
                else
                {
                    moves.Add(Coordinate.South);
                    up = -1;
                    promotion_Y = 0;
                }
                for (int i = -1; i < 2; i += 2)
                {
                    if (ChessBoard.instance.InBoard(this.coordinate + new Coordinate(i, up)))
                    {
                        Contest(ChessBoard.instance.GetTileFromCoord(this.coordinate + new Coordinate(i, up)));
                        if (ChessBoard.instance.GetPieceFromCoord(this.coordinate + new Coordinate(i, up)))
                            moves.Add(new Coordinate(i, up));

                    }
                }
                return moves.ToArray();
            }
        }
        public override void Move(Coordinate c, bool theoretical)
        {
            if (!theoretical)
                if (!tookFirstTurn)
                {
                    if (this.coordinate + Coordinate.North * 2 == c || this.coordinate + Coordinate.South * 2 == c)
                    {
                        int previous;
                        if (isWhite)
                            previous = 1;
                        else
                            previous = -1;
                        enPassed = ChessBoard.instance.tiles[this.coordinate.x, this.coordinate.y + previous];
                        enPassed.currentPiece = this;
                    }
                    tookFirstTurn = true;
                }
            base.Move(c, theoretical);
        }
        private void RemoveEnPassed()
        {
            if (enPassed != null)
            {
                if (Controller.instance.whitehasTurn == this.isWhite)
                {
                    if (enPassed.currentPiece == this)
                        enPassed.currentPiece = null;
                    enPassed = null;
                }
            }
        }
        public override void CheckMoves()
        {
            RemoveEnPassed();
            base.CheckMoves();
        }
    }
}