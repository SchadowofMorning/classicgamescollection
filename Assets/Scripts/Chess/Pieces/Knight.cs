﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Chess
{
    public class Knight : Piece
    {

        public override int repetitions
        {
            get
            {
                return 1;
            }
        }
        public override Coordinate[] MovePattern
        {
            get
            {
                return new Coordinate[]
                {
                new Coordinate(2,1),
                new Coordinate(2,-1),
                new Coordinate(1,2),
                new Coordinate(1,-2),
                new Coordinate(-2,1),
                new Coordinate(-2,-1),
                new Coordinate(-1,2),
                new Coordinate(-1,-2)
                };
            }
        }
    }
}