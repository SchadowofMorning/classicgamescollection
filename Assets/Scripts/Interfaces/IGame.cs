﻿
using Newtonsoft.Json.Linq;
public interface IGame
{
    void UpdateState(string data);
    void StartGame(object o, Networker net);
}
