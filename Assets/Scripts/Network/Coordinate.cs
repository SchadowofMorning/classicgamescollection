﻿using System;
using UnityEngine;
using Newtonsoft.Json;
[Serializable]
public struct Coordinate
{
    public int x;
    public int y;
    [JsonIgnore()]
    public Vector3 vector
    {
        get
        {
            return new Vector3(x, 0, y);
        }
    }
    public Coordinate(int X, int Y)
    {
        x = X;
        y = Y;
    }
    /// <summary>
    /// (0,1)
    /// </summary>
    public static Coordinate North { get { return new Coordinate(0, 1); } }
    /// <summary>
    /// (0,-1)
    /// </summary>
    public static Coordinate South { get { return new Coordinate(0, -1); } }
    /// <summary>
    /// (-1,0)
    /// </summary>
    public static Coordinate West { get { return new Coordinate(-1, 0); } }
    /// <summary>
    /// (1,0)
    /// </summary>
    public static Coordinate East { get { return new Coordinate(1, 0); } }
    /// <summary>
    /// (0,0)
    /// </summary>
    public static Coordinate zero { get { return new Coordinate(0, 0); } }
    public static Coordinate[] StandardDistance
    {
        get
        {
            return new Coordinate[] { North, South, West, East };
        }
    }

    public static Coordinate VecToCoordinate(Vector3 input)
    {
        int x = (int)Math.Round(input.x);
        int y = (int)Math.Round(input.y);
        return new Coordinate(x, y);
    }
    public float Distance(Coordinate target)
    {
        return Mathf.Sqrt(Mathf.Pow(target.x - this.x, 2) + Mathf.Pow(target.y - this.y, 2));
    }
    public static bool operator ==(Coordinate c1, Coordinate c2)
    {
        return (c1.x == c2.x && c1.y == c2.y);
    }
    public static bool operator !=(Coordinate c1, Coordinate c2)
    {
        return !(c1.x == c2.x && c1.y == c2.y);
    }
    public static bool operator <(Coordinate c1, Coordinate c2)
    {
        return ((c1.x + c1.y) < (c2.x + c2.y));
    }
    public static bool operator >(Coordinate c1, Coordinate c2)
    {
        return ((c1.x + c1.y) > (c2.x + c2.y));
    }
    public static Coordinate operator +(Coordinate c1, Coordinate c2)
    {
        return new Coordinate(c1.x + c2.x, c1.y + c2.y);
    }
    public static Coordinate operator -(Coordinate c1, Coordinate c2)
    {
        return new Coordinate(c1.x - c2.x, c1.y - c2.y);
    }
    public static Coordinate operator *(Coordinate c1, Coordinate c2)
    {
        return new Coordinate(c1.x * c2.x, c1.y * c2.y);
    }
    public static Coordinate operator *(Coordinate c, int i)
    {
        return new Coordinate(c.x * i, c.y * i);
    }
    /// <summary>
    /// Determins if variable b is in Range r of variable a
    /// </summary>
    /// <param name="a"></param>
    /// <param name="b"></param>
    /// <param name="r"></param>
    /// <returns></returns>
    public static bool InRange(Coordinate a, Coordinate b, int r)
    {
        return (((a + (Coordinate.West * r)).x < b.x && (a + (Coordinate.East * r)).x > b.x) && ((a + (Coordinate.North * r)).y > b.y && (a + (Coordinate.South * r)).y < b.y));
    }
    public override bool Equals(object obj)
    {
        return base.Equals(obj);
    }
    public override int GetHashCode()
    {
        return base.GetHashCode();
    }
    public override string ToString()
    {
        return ("[" + x.ToString() + ", " + y.ToString() + "]");
    }
}
