﻿
using System;
using System.Collections.Generic;
using UnityEngine;
[Serializable]
public class User
{
    public bool anonymous { get { return password == null; } }
    public string id;
    public string password { get;private set; }
    public bool authorized { get; private set; }
    public int GOMMR;
    public User()
    {

    }
    public User(string name, string _password = "")
    {
        id = name;
        password = _password;
    }
    public override string ToString()
    {
        return JsonUtility.ToJson(this);
    }
}    
   