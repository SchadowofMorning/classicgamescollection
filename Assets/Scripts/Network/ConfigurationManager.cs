﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
public class ConfigurationManager : MonoBehaviour
{
    public JObject config;
    private string svrstring;
    private FileStream fs;
    [SerializeField]
    private TMP_InputField serverAddressField;
    public void InitializeConfiguration()
    {
        fs = File.Open(Application.persistentDataPath + "/config.json", FileMode.OpenOrCreate);
        Debug.Log(Application.persistentDataPath + "/config.json");
        using (StreamReader sr = new StreamReader(fs))
        {
            string result = sr.ReadToEnd();
            try
            {
                config = JsonConvert.DeserializeObject<JObject>(result);
            }
            catch (Exception e)
            {
                config = new JObject();
                config["connectionstring"] = "ws://localhost:3000";
                using (StreamWriter sw = new StreamWriter(fs))
                {
                    sw.Write(JsonConvert.SerializeObject(config));
                }
            }
        }
        serverAddressField.text = config["connectionstring"].ToString();
    }
    public void Toggle()
    {
        this.gameObject.SetActive(!gameObject.activeSelf);
    }
    public void Update()
    {
        if (serverAddressField.text != config["connectionstring"].ToString())
        {
            config["connectionstring"] = serverAddressField.text;
            fs = File.Open(Application.persistentDataPath + "/config.json", FileMode.OpenOrCreate);
            using (StreamWriter sw = new StreamWriter(fs))
            {
                sw.Write(JsonConvert.SerializeObject(config));
            }
            Debug.Log("updated config");
        }
    }
}