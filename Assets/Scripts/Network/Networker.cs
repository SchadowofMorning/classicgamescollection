﻿#define Debug
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using WebSocketSharp;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.IO;

/// <summary>
/// Networker Class that opens and manages connections
/// For Debugging uncomment first line
/// </summary>
public class Networker : MonoBehaviour
{
    public ConfigurationManager configManager;
    private WebSocket socket;
    /// <summary>
    /// The Event you can subscribe a function to, events return a type(string) and a json string you can parse with JsonUtility
    /// </summary>
    public event Action<string, string> OnMessage;
    /// <summary>
    /// Dictionary that evokes callbacks on messages
    /// </summary>
    private Dictionary<string, Action<string>[]> callbacks;
    /// <summary>
    /// If websockets are alive and well this returns true
    /// </summary>
    public bool alive
    {
        get
        {
            return socket.IsAlive;
        }
    }
    private void Awake()
    {
        configManager.InitializeConfiguration();
        InitializeSocket();
    }
    /// <summary>
    /// Initialize WebSocket and add Handle class
    /// </summary>
    public void InitializeSocket()
    {
        callbacks = new Dictionary<string, Action<string>[]>();
        OnMessage = delegate { };
        try
        {
            socket = new WebSocket(configManager.config["connectionstring"].ToString());
        }
        catch (ArgumentException e)
        {
            Debug.LogError(e);
            configManager.config["connectionstring"] = "ws://localhost:3000";
            socket = new WebSocket(configManager.config["connectionstring"].ToString());
        } finally
        {
            socket.OnMessage += Handle;
        }
    }
    public void OnApplicationQuit()
    {
        socket.Close();
    }
    /// <summary>
    /// Open the socket, should only be done once
    /// </summary>
    public void OpenSocket()
    {
        socket = new WebSocket(configManager.config["connectionstring"].ToString());
        socket.OnMessage += Handle;
        socket.Connect();
    }
    /// <summary>
    /// never forget to end shit properly
    /// </summary>
    public void CloseSocket()
    {
        socket.Close();
#if (Debug)
        Debug.Log("Socket closed");
        Debug.Log(socket.ReadyState);
#endif
    }
    /// <summary>
    /// function that you should not use, its only public so the event can see it properly
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="args"></param>
    public void Handle(object sender, MessageEventArgs args)
    {
        var dict = JsonConvert.DeserializeObject<Dictionary<string, string>>(args.Data);
        string type = dict["type"];
        string data = dict["data"];
#if (Debug)
        Debug.Log(string.Format("Received {0} with data: {1}", type, data));
#endif
        OnMessage(type, data);
        HandleCallbacks(type, data);
    }
    /// <summary>
    /// Send A Message to the server
    /// </summary>
    /// <param name="type">types that have to be specifically implemented in the server</param>
    /// <param name="data">json data you want to send, I recommend to use JsonUtility.ToJson() or JsonConvert</param>
    public void SocketMessage(string type, string data)
    {
#if (Debug)
        Debug.Log(string.Format("Send {0} with data: {1}", type, data));
#endif
        socket.Send(string.Format("{{ \"type\":\"{0}\", \"data\":{1} }}", type, data));
    }
    /// <summary>
    /// Bind a delegate to a certain event from the server
    /// </summary>  
    /// <param name="type"></param>
    /// <param name="callback"></param>
    public void On(string type, Action<string> callback)
    {
        if (callbacks.Keys.Contains(type))
        {
            Action<string>[] old = callbacks[type];
            List<Action<string>> list = new List<Action<string>>(old);
            list.Add(callback);
            callbacks[type] = list.ToArray();
        } else
        {
            callbacks[type] = new Action<string>[] { callback };
        }
    }
    /// <summary>
    /// Gets Invoked when a message is received
    /// </summary>
    /// <param name="type"></param>
    /// <param name="data"></param>
    public void HandleCallbacks(string type, string data)
    {
        Action<string>[] cbs;
        if(callbacks.TryGetValue(type, out cbs))
        {

            foreach (Action<string> act in cbs)
            {
#if (Debug)
                Debug.LogFormat("Calling function {0} of event-type {1}", act.Method.Name, type);
#endif
                act(data);
            }
        }
    }
}
