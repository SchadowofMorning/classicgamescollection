﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


namespace GO
{
    public class LoadReplayButton : MonoBehaviour
    {

        public void Click()
        {
            string replay = this.GetComponentInChildren<Text>().text.Replace(" ", "");
            string path = "Assets/Resources/~Replays/" + replay + ".txt";
            Debug.Log("Loading: " + replay);
            ReplaySystem.replay_system.LoadReplay(path);
        }
    }
}