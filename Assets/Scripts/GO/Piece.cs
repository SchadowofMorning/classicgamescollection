﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GO
{
    public class Piece : MonoBehaviour
    {
        public Tile tile;
        public int color; // 0 = white ; 1 = black
        public Connection connection;
        public Logic logic;

        void Start()
        {
            logic = GameObject.Find("Logic").GetComponent<Logic>();
        }

        void OnMouseUp()
        {
            if (this == Logic.moving_piece && TurnManager.myTurn)
            {
                Sitdown(false, logic);
            }
        }
        public void Sitdown(bool byNetwork, Logic l)
        {
            logic = l;
            Tile closest_tile = Logic.GetClosestTile(this.transform.position);
            bool suicide = false;
            bool ko_rule = false;
            if (closest_tile != null)
            {
                this.connection = new Connection(this);
                this.transform.position = closest_tile.piece_position;
                this.tile = closest_tile;
                this.tile.piece = this;

                ArrayList cons = this.connection.GetSurroundingConnections(this.color, this.tile);
                List<Connection> surrounding_connections = (List<Connection>)cons[0];
                List<Connection> surrounding_connections_other = (List<Connection>)cons[1];

                foreach (Connection con in surrounding_connections)
                {
                    foreach (Piece p in con)
                    {
                        this.connection.Add(p);
                        p.connection = this.connection;
                    }
                }

                if (!this.connection.HasLiberties())
                    suicide = true;

                if (Logic.ko_position != this.transform.position)
                    Logic.ko_position = Vector3.negativeInfinity;

                List<Vector3> pieces_removed = new List<Vector3>();

                foreach (Connection con in surrounding_connections_other)
                {
                    if (!con.HasLiberties())
                    {
                        suicide = false;
                        if (con.Count == 1)
                        {
                            if (Logic.ko_position == this.transform.position)
                            {
                                TurnManager.DoTurn(this.color, "ko", Vector3.zero, byNetwork);
                                logic.StartScrollDown();
                                ko_rule = true;
                                this.Remove();
                            }
                            else
                            {
                                pieces_removed.Add(con[0].tile.piece_position);
                                Destroy(con[0].gameObject);
                                con[0].tile.piece = null;
                                Logic.ko_position = con[0].transform.position;
                                Logic.points[this.color]++;
                            }
                        }
                        else
                        {
                            foreach (Piece p in con)
                            {
                                pieces_removed.Add(p.tile.piece_position);
                                Destroy(p.gameObject);
                                p.tile.piece = null;
                                Logic.points[this.color]++;
                            }
                        }
                    }
                }

                if (suicide)
                {
                    TurnManager.DoTurn(this.color, "suicide", Vector3.zero, byNetwork);
                    logic.StartScrollDown();
                    this.Remove();
                    return;
                }

                if (ko_rule)
                    return;

                ReplaySystem.replay_system.SaveTurn(this.color, this.tile.piece_position, pieces_removed);
                Logic.moving_piece = null;

                if (this.color == 0)
                {
                    TurnManager.turn = 1;
                    TurnManager.DoTurn(this.color, "turn", this.tile.transform.position, byNetwork);
                    logic.StartScrollDown();
                    this.GetComponent<Renderer>().material.color = new Color(1, 1, 1, 1);
                }
                else
                {
                    TurnManager.turn = 0;
                    TurnManager.DoTurn(this.color, "turn", this.tile.transform.position, byNetwork);
                    logic.StartScrollDown();
                    this.GetComponent<Renderer>().material.color = new Color(0, 0, 0, 1);
                }
            }
        }
        void Remove()
        {
            this.connection.Remove(this);
            this.tile.piece = null;
            Destroy(this.gameObject);
        }
    }

    public class Connection : List<Piece>
    {
        public int color = 0;
        public Connection(Piece piece)
        {
            this.Add(piece);
            this.color = piece.color;
        }

        public ArrayList GetSurroundingConnections(int color, Tile tile)
        {
            List<Connection> surrounding_connections = new List<Connection>();
            List<Connection> surrounding_connections_other = new List<Connection>();
            foreach (Tile t in tile.surroundings)
            {
                if (t.piece != null)
                {
                    if (t.piece.color == color)
                    {
                        surrounding_connections.Add(t.piece.connection);
                    }
                    else
                    {
                        surrounding_connections_other.Add(t.piece.connection);
                    }
                }
            }

            ArrayList al = new ArrayList(2);
            al.Add(surrounding_connections);
            al.Add(surrounding_connections_other);
            return al;
        }

        public bool HasLiberties()
        {
            bool liberties = false;
            foreach (Piece p in this)
            {
                if (p.tile == null)
                    continue;

                foreach (Tile t in p.tile.surroundings)
                {
                    if (t.piece == null)
                    {
                        liberties = true;
                        break;
                    }
                }
                if (liberties == true)
                    break;
            }

            return liberties;
        }
    }
}