﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System;
using UnityEngine;
using UnityEngine.UI;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;


namespace GO
{
    public class ReplaySystem : MonoBehaviour
    {
        public static ReplaySystem replay_system;
        public Logic logic;
        public GoReplay replay = new GoReplay();
        public List<Turn> current_turns = new List<Turn>();
        public int current_turn = 0;
        public GameObject ControlReplayPanel;
        public Button PlayButton;
        public Button PauseButton;
        public bool paused = false;

        void Start()
        {
            replay_system = this;
        }

        public void SaveReplay()
        {
            replay.board_size = logic.board_size;
            replay.turns = current_turns;
            string name = System.DateTime.Now.Year + "_" + System.DateTime.Now.Month + "_" + System.DateTime.Now.Day + "-" + System.DateTime.Now.Hour + "_" + System.DateTime.Now.Minute + "_" + System.DateTime.Now.Second;
            string path = "Assets/Resources/~Replays/" + name + ".txt";
            string json = JsonUtility.ToJson(replay); // JsonConvert.SerializeObject(replay, Formatting.Indented);
            StreamWriter writer = new StreamWriter(path, true);
            writer.Write(json);
            writer.Close();
            Debug.Log("Saved file " + path);
        }

        public void LoadReplay(string path)
        {
            StreamReader reader = new StreamReader(path);
            string json = reader.ReadToEnd();
            replay = JsonConvert.DeserializeObject<GoReplay>(json);
            logic.board_size = replay.board_size;
            logic.StartNewGame(true);
            paused = false;
            StartCoroutine(Play());

            if (Logic.territory_check)
                Logic.game_logic.CheckTerritories();

        }

        public void SaveTurn(int color, Vector3 position, List<Vector3> pieces_removed, int action=0)
        {
            Turn t = new Turn(color, position, pieces_removed, action);
            current_turns.Add(t);
        }

        public void PlayReplay()
        {
            paused = false;
            StartCoroutine(Play());
        }

        public IEnumerator Play()
        {
            if (!paused)
            {
                yield return new WaitForEndOfFrame();
                Next();

                if (current_turn <= replay.turns.Count)
                {
                    yield return new WaitForSeconds(2f);
                    StartCoroutine(Play());
                }
            }
        }

        public void Pause()
        {
            if (!paused)
            {
                paused = true;
                StopCoroutine(Play());
                PauseButton.gameObject.SetActive(false);
                PlayButton.gameObject.SetActive(true);
            }
        }

        public void Next(bool stop_playing = false)
        {
            if (stop_playing)
                Pause();

            if (current_turn + 1 <= replay.turns.Count)
            {
                Turn turn = replay.turns[current_turn];
                if (turn.action == 0)
                {
                    foreach (Tile tile in Logic.tiles)
                    {
                        if (tile.piece_position == turn.position)
                        {
                            Piece piece = Instantiate(Resources.Load<Piece>("Prefabs/Piece"));
                            piece.transform.position = turn.position;
                            piece.color = turn.color;
                            piece.tile = tile;
                            tile.piece = piece;

                            if (piece.color == 0)
                            {
                                piece.GetComponent<Renderer>().material.color = new Color(1, 1, 1, 1);
                            }
                            else
                            {
                                piece.GetComponent<Renderer>().material.color = new Color(0, 0, 0, 1);
                            }

                            TurnManager.DoTurn(piece.color, "turn", piece.tile.transform.position);
                            logic.StartScrollDown();
                        }

                        if (turn.pieces_removed.Count > 0)
                        {
                            if (tile.piece != null && turn.pieces_removed.Contains(tile.piece_position))
                            {
                                Destroy(tile.piece.gameObject);
                                Logic.points[turn.color]++;
                            }
                        }
                    }
                }
                else if (turn.action == 1)
                {
                    TurnManager.DoTurn(turn.color, "pass", Vector3.zero);
                    logic.StartScrollDown();
                }
                else if (turn.action == -1)
                {
                    TurnManager.DoTurn(turn.color, "resign", Vector3.zero);
                    logic.StartScrollDown();
                }

                current_turn++;

                if ((current_turn >= replay.turns.Count) && turn.action != -1)
                {
                    TurnManager.DoTurn(0, "game over replay", Vector3.zero);
                }
            }
        }

        public void Previous()
        {
            Pause();
            if (Logic.territory_check)
            {
                Logic.game_logic.CheckTerritories();
            }

            if (current_turn > 0)
            {
                Turn last_turn = replay.turns[current_turn - 1];

                foreach (Tile tile in Logic.tiles)
                {
                    if (tile.piece != null && tile.piece_position == last_turn.position)
                    {
                        Destroy(tile.piece.gameObject); 
                    }

                    if (last_turn.pieces_removed.Count > 0)
                    {
                        foreach (Vector3 vec in last_turn.pieces_removed)
                        {
                            if (vec == tile.piece_position)
                            {
                                Piece piece = Instantiate(Resources.Load<Piece>("Prefabs/Piece"));
                                piece.transform.position = vec;
                                if (last_turn.color == 1)
                                {
                                    piece.color = 0;
                                    piece.GetComponent<Renderer>().material.color = new Color(1, 1, 1, 1);
                                }
                                else
                                {
                                    piece.color = 1;
                                    piece.GetComponent<Renderer>().material.color = new Color(0, 0, 0, 1);
                                }
                                piece.tile = tile;
                                tile.piece = piece;
                                Logic.points[last_turn.color]--;
                            }
                        }
                    }
                }

                current_turn--;
                StartCoroutine(TurnManager.RemoveLastUIScrollText());

            }
        }
    }

    [Serializable]
    public class GoReplay
    {
        public List<Turn> turns = new List<Turn>();
        public int board_size;
        public int black_points;
        public float white_points;

        public void New()
        {
            turns.Clear();
            board_size = Logic.game_logic.board_size;
            white_points = 0;
            black_points = 0;
        }
    }

    [Serializable]
    public struct Turn
    {
        public int color;
        public Vector3 position;
        public List<Vector3> pieces_removed;
        public int action;


        public Turn(int _color, Vector3 _position, List<Vector3> _pieces_removed, int _action=0)
        {
            color = _color;
            position = _position;
            pieces_removed = _pieces_removed;
            action = _action;
        }
    }
}


