﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GO
{
    public class Tile : MonoBehaviour
    {

        public Piece piece;
        public Vector3 piece_position;
        public List<Tile> surroundings = new List<Tile>();
        void Start()
        {
            piece_position = new Vector3(this.transform.position.x - 0.5f, this.transform.localScale.y / 2, this.transform.position.z - 0.5f);
        }

        public IEnumerator GetSurroundingTiles()
        {
            yield return new WaitForEndOfFrame(); // gotta wait before all Tiles are fully created
            Vector3 pos = piece_position;
            pos.x -= 1f;
            Tile t = Logic.GetTileByPiecePosition(pos);
            if (t)
                surroundings.Add(t);

            pos.x += 2f;
            t = Logic.GetTileByPiecePosition(pos);
            if (t)
                surroundings.Add(t);

            pos.x = piece_position.x;
            pos.z -= 1f;
            t = Logic.GetTileByPiecePosition(pos);
            if (t)
                surroundings.Add(t);

            pos.z += 2f;
            t = Logic.GetTileByPiecePosition(pos);
            if (t)
                surroundings.Add(t);
        }

    }

}