﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

namespace GO
{
    public class Logic : MonoBehaviour, IGame
    {
        public static Logic game_logic;
        public ReplaySystem replay_system;
        public int board_size;
        public GameObject board;
        public static List<Tile> tiles = new List<Tile>();
        public static Vector3 ko_position;
        public static List<Territory> territories = new List<Territory>();
        public static List<GameObject> territory_objects = new List<GameObject>();
        public static bool territory_check = false;
        public static Piece moving_piece = null;
        public Scrollbar scrollbar;
        public static GameObject scroll_content;
        public Canvas board_canvas;
        public static string[] letters = new string[20] { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T" };
        public static int[] points = new int[2] { 0, 0 };
        public static int white_area_points = 0;
        public static int black_area_points = 0;
        public static int black_points = 0;
        public static bool game_over = false;
        public static bool in_replay = false;
        public Button pass_button;
        public Button resign_button;
        public Text check_territory_text;

        public Networker network;
        public int myColor;
        private Queue<Tuple<int, string, Vector3, bool>> todoMoves;

        private void OnValidate()
        {
            board_size = Mathf.Clamp(board_size, 5, 19);
        }

        void Initialize()
        {
            todoMoves = new Queue<Tuple<int, string, Vector3, bool>>();
            TurnManager.network = network;
            TurnManager.system = replay_system;
            TurnManager.myColor = myColor;
            game_logic = this;
            scroll_content = GameObject.Find("Content");
            CreateBoard();
            TurnManager.network.SocketMessage("game-update", JsonUtility.ToJson(TurnManager.system.current_turns));
        }

        void Update()
        {
            if (todoMoves.Count > 0)
            {
                Tuple<int, string, Vector3, bool> tuple = todoMoves.Dequeue();
                DoMove(tuple);
            }
            if (game_over || in_replay || TurnManager.turn != myColor)
                return;

            if (moving_piece == null)
            {
                moving_piece = Instantiate(Resources.Load<Piece>("Prefabs/Piece"));
                if (TurnManager.turn == 0)
                {
                    moving_piece.color = 0;
                    moving_piece.GetComponent<Renderer>().material.color = new Color(1, 1, 1, 0.3f);
                }
                else
                {
                    moving_piece.color = 1;
                    moving_piece.GetComponent<Renderer>().material.color = new Color(0, 0, 0, 0.3f);
                }
            }

            Vector3 mouse_pos = Input.mousePosition;
            mouse_pos.z = Camera.main.transform.position.y;
            mouse_pos = Camera.main.ScreenToWorldPoint(mouse_pos);
            mouse_pos.y = 0.5f;

            Tile t = GetClosestTile(mouse_pos);
            
            if (t == null)
                moving_piece.gameObject.SetActive(false);
            else
            {
                moving_piece.gameObject.SetActive(true);
                moving_piece.transform.position = t.piece_position;
            }
            
        }

        public void CreateBoard()
        {
            float half_size = board_size / 2f - 0.5f;

            board_canvas.transform.position = new Vector3(half_size, 0.51f, half_size);
            board_canvas.GetComponent<RectTransform>().sizeDelta = new Vector2(board_size + 2, board_size + 2);

            // Create Tiles
            for (int x = 0; x <= board_size; x++)
            {
                for (int y = board_size; y >= 0; y--)
                {
                    Tile tile;
                    if (x == board_size || y == board_size)
                        tile = new GameObject().AddComponent<Tile>();
                    else
                        tile = Instantiate(Resources.Load<Tile>("Prefabs/Tile"));

                    if (x == 0)
                    {
                        Text position_indicator = Instantiate(Resources.Load<Text>("Prefabs/PositionIndicator"));
                        position_indicator.transform.SetParent(board_canvas.transform);
                        RectTransform rt = position_indicator.GetComponent<RectTransform>();
                        rt.anchoredPosition3D = new Vector3(0.23f, y + 1, 0);
                        rt.localEulerAngles = Vector3.zero;
                        position_indicator.text = (y + 1).ToString();
                    }

                    if (y == 0)
                    {
                        Text position_indicator = Instantiate(Resources.Load<Text>("Prefabs/PositionIndicator"));
                        position_indicator.transform.SetParent(board_canvas.transform);
                        RectTransform rt = position_indicator.GetComponent<RectTransform>();
                        rt.anchoredPosition3D = new Vector3(x + 1, 0.23f, 0);
                        rt.localEulerAngles = Vector3.zero;
                        position_indicator.text = letters[x];
                    }

                    tile.transform.position = new Vector3(x, 0, y);
                    tile.transform.SetParent(board.transform);
                    tiles.Add(tile);
                    StartCoroutine(tile.GetSurroundingTiles());
                }
            }

            // Create border
            GameObject bottom = (GameObject)Instantiate(Resources.Load("Prefabs/Tile"));
            bottom.transform.position = new Vector3(-1, 0, half_size);
            bottom.transform.localScale = new Vector3(1, 1, board_size + 2);
            bottom.GetComponent<Renderer>().material = (Material)Resources.Load("Materials/border_texture");
            bottom.transform.SetParent(board.transform);

            GameObject up = (GameObject)Instantiate(Resources.Load("Prefabs/Tile"));
            up.transform.position = new Vector3(board_size, 0, half_size);
            up.transform.localScale = new Vector3(1, 1, board_size + 2);
            up.GetComponent<Renderer>().material = (Material)Resources.Load("Materials/border_texture");
            up.transform.SetParent(board.transform);

            GameObject left = (GameObject)Instantiate(Resources.Load("Prefabs/Tile"));
            left.transform.position = new Vector3(half_size, 0, -1);
            left.transform.localScale = new Vector3(1, 1, board_size);
            left.transform.localEulerAngles = new Vector3(0, 90, 0);
            left.GetComponent<Renderer>().material = (Material)Resources.Load("Materials/border_texture");
            left.transform.SetParent(board.transform);

            GameObject right = (GameObject)Instantiate(Resources.Load("Prefabs/Tile"));
            right.transform.position = new Vector3(half_size, 0, board_size);
            right.transform.localScale = new Vector3(1, 1, board_size);
            right.transform.localEulerAngles = new Vector3(0, 90, 0);
            right.GetComponent<Renderer>().material = (Material)Resources.Load("Materials/border_texture");
            right.transform.SetParent(board.transform);

            float extra_cam_height = 2f;
            if (board_size > 8)
                extra_cam_height = 1.5f;
            if (board_size > 12)
                extra_cam_height = 1f;
            if (board_size > 15)
                extra_cam_height = 0.5f;

            Camera.main.transform.position = new Vector3(half_size, board_size + extra_cam_height, half_size);

        }

        public static Tile GetTileByPiecePosition(Vector3 pos)
        {
            foreach (Tile t in tiles)
            {
                if (t.piece_position == pos)
                    return t;
            }

            return null;
        }

        public static Tile GetClosestTile(Vector3 pos)
        {
            foreach (Tile t in tiles)
            {
                if (Vector3.Distance(pos, t.piece_position) < 0.6f && !t.piece)
                {
                    return t;
                }
            }
            return null;
        }

        public void Pass()
        {
            if (game_over)
                return;

            if (TurnManager.turn == 0)
            {
                replay_system.SaveTurn(TurnManager.turn, Vector3.zero, null, 1);
                TurnManager.DoTurn(TurnManager.turn, "pass", Vector3.zero);
                TurnManager.turn = 1;
                moving_piece.GetComponent<Renderer>().material.color = new Color(0, 0, 0, 0.3f);
                moving_piece.color = 1;
            }
            else
            {
                replay_system.SaveTurn(TurnManager.turn, Vector3.zero, null, 1);
                TurnManager.DoTurn(TurnManager.turn, "pass", Vector3.zero);
                TurnManager.turn = 0;
                moving_piece.GetComponent<Renderer>().material.color = new Color(1, 1, 1, 0.3f);
                moving_piece.color = 0;
            }
            StartScrollDown();
        }

        public void Resign()
        {
            if (game_over)
                return;

            if (TurnManager.turn == 0)
            {
                replay_system.SaveTurn(TurnManager.turn, Vector3.zero, null, -1);
                TurnManager.DoTurn(TurnManager.turn, "resign", Vector3.zero);
                TurnManager.turn = 1;
                game_over = true;
            }
            else
            {
                replay_system.SaveTurn(TurnManager.turn, Vector3.zero, null, -1);
                TurnManager.DoTurn(TurnManager.turn, "resign", Vector3.zero);
                TurnManager.turn = 0;
                moving_piece.GetComponent<Renderer>().material.color = new Color(1, 1, 1, 0.3f);
                moving_piece.color = 0;
            }
            game_over = true;
            ReplaySystem.replay_system.SaveReplay();
            StartScrollDown();
        }

        public void StartNewGame(bool replay=false)
        {
            StopCoroutine(replay_system.Play());

            foreach (Tile t in tiles)
            {
                if (t.piece != null)
                    Destroy(t.piece.gameObject);
            }

            foreach (Transform t in board.transform)
            {
                if (t == board.transform)
                    continue;
                Destroy(t.gameObject);
            }
            tiles.Clear();

            if (moving_piece != null)
                Destroy(moving_piece);

            ko_position = Vector3.negativeInfinity;
            points = new int[2] { 0, 0 };

            foreach (Transform t in scroll_content.transform)
            {
                if (t == scroll_content.transform)
                    continue;
                Destroy(t.gameObject);
            }

            foreach (Transform t in board_canvas.transform)
            {
                if (t == board_canvas.transform)
                    continue;
                Destroy(t.gameObject);
            }

            foreach (GameObject go in territory_objects)
            {
                Destroy(go);
            }
            territory_objects.Clear();
            territory_check = false;
            TurnManager.turn = 1;
            game_over = false;
            if (!replay)
            {
                replay_system.paused = true;
                replay_system.ControlReplayPanel.SetActive(false);
                pass_button.gameObject.SetActive(true);
                resign_button.gameObject.SetActive(true);
                replay_system.replay.New();
                in_replay = false;
            }
            else
            {
                replay_system.ControlReplayPanel.SetActive(true);
                pass_button.gameObject.SetActive(false);
                resign_button.gameObject.SetActive(false);
                in_replay = true;
            }

            CreateBoard();

        }

        public void ViewReplays()
        {
            foreach (Transform t in scroll_content.transform)
            {
                if (t == scroll_content.transform)
                    continue;
                Destroy(t.gameObject);
            }

            string path = "Assets/Resources/~Replays/";
            foreach (string file in Directory.GetFiles(path))
            {
                if (file.EndsWith(".txt"))
                {
                    string text = file.Replace("Assets/Resources/~Replays\\", "");
                    text = text.Replace(".txt", "");
                    AddUIScrollButton(text);
                }
            }
        }

        public void CheckTerritories()
        {
            if (game_over)
                return;

            if (territory_check == false)
            {
                check_territory_text.text = "Check Territory: On";
                territory_check = true;
                territories.Clear();
                foreach (Tile t in tiles)
                {
                    if (t.piece != null)
                        continue;

                    List<Territory> surrounding_territories = new List<Territory>();
                    foreach (Tile t2 in t.surroundings)
                    {
                        if (t2.piece != null)
                            continue;

                        foreach (Territory tr in territories)
                        {
                            if (tr.Contains(t2))
                            {
                                if (!surrounding_territories.Contains(tr))
                                    surrounding_territories.Add(tr);
                            }
                        }
                    }

                    Territory territory = new Territory();
                    foreach (Territory tr in surrounding_territories)
                    {
                        foreach (Tile t2 in tr)
                        {
                            territory.Add(t2);
                        }
                        tr.Clear();
                        territories.Remove(tr);
                    }
                    territory.Add(t);
                    territories.Add(territory);
                }

                white_area_points = 0;
                black_area_points = 0;

                foreach (Territory territory in territories)
                {
                    if (territory.Count == 0)
                        continue;

                    bool has_white = false;
                    bool has_black = false;

                    foreach (Tile t in territory)
                    {
                        foreach (Tile t2 in t.surroundings)
                        {
                            if (t2.piece == null)
                                continue;
                            if (t2.piece.color == 0)
                                has_white = true;
                            if (t2.piece.color == 1)
                                has_black = true;
                        }
                    }

                    foreach (Tile t in territory)
                    {
                        if (has_white && has_black)
                            continue;

                        GameObject go = (GameObject)Instantiate(Resources.Load("Prefabs/Territory"));
                        go.transform.position = t.piece_position;
                        if (has_white && !has_black)
                        {
                            go.GetComponent<Renderer>().material.color = Color.white;
                            territory.color = 0;
                            white_area_points++;
                        }
                        if (has_black && !has_white)
                        {
                            go.GetComponent<Renderer>().material.color = Color.black;
                            territory.color = 1;
                            black_area_points++;
                        }
                        territory_objects.Add(go);
                    }
                }

                TurnManager.DoTurn(0, "area points", new Vector3(white_area_points, black_area_points, 0));
                StartScrollDown();

            }
            else
            {
                foreach (GameObject go in territory_objects)
                {
                    Destroy(go);
                }
                territory_check = false;
                check_territory_text.text = "Check Territory: Off";
            }
        }

        public void StartScrollDown()
        {
            StartCoroutine(ScrollDown());
        }

        IEnumerator ScrollDown()
        {
            yield return new WaitForEndOfFrame();
            yield return new WaitForEndOfFrame();
            scrollbar.value = 0;
        }

        public void AddUIScrollButton(string text)
        {
            Button button = GameObject.Instantiate(Resources.Load<Button>("Prefabs/UIScrollButton"));
            button.GetComponentInChildren<Text>().text = " " + text;
            button.transform.SetParent(Logic.scroll_content.transform);
        }

        public void UpdateState(string data)
        {
            JObject jObj = JsonConvert.DeserializeObject<JObject>(data);
            Debug.Log(jObj["new_val"]["turns"]);
            try
            {
                if (jObj["new_val"]["turns"].ToString() == "{}") return;
                List<Turn> turns = jObj["new_val"]["turns"].ToObject<Turn[]>().ToList();
                for (int i = 0; i < turns.Count; i++)
                {
                    if (i > replay_system.current_turns.Count - 1)
                    {
                        string action;
                        switch (turns[i].action)
                        {
                            case -1:
                                action = "resign";
                                break;
                            case 1:
                                action = "pass";
                                break;
                            default:
                                action = "turn";
                                break;
                        }
                        var t = new Tuple<int, string, Vector3, bool>(turns[i].color, action, turns[i].position, true);
                        todoMoves.Enqueue(t);
                    }
                }
            } catch(Exception e)
            {
                Debug.LogError(e.Message);
            }
        }
        public void DoMove(Tuple<int, string, Vector3, bool> input)
        {
            DoMove(input.Item1, input.Item2, input.Item3, input.Item4);
        }
        public void DoMove(int color, string action, Vector3 pos, bool byNetwork)
        {
            switch (action)
            {
                case "turn":
                    Piece prefab = Resources.Load<Piece>("Prefabs/Piece");
                    Piece instance = Instantiate(prefab);
                    instance.transform.position = pos;
                    instance.color = color;
                    instance.Sitdown(byNetwork, this);
                    break;
                case "resign":
                    Resign();
                    TurnManager.AddUIScrollText((color == 0 ? "White" : "Black") + " resigned",color == 0 ? Color.white : Color.black);
                    TurnManager.AddUIScrollText((color == 0 ? "Black" : "White") + " wins!", Color.green);
                    break;
                case "pass":
                    Pass();
                    TurnManager.AddUIScrollText((color == 0 ? "White" : "Black") + " passed", color == 0 ? Color.white : Color.black);
                    break;
            }
        }

        public void StartGame(object o, Networker net)
        {
            myColor = (int)o;
            network = net;
            Initialize();
        }
    }

    public class Territory : List<Tile>
    {
        public int color = -1;
    }

    public static class TurnManager
    {
        // black starts;
        public static int turn = 1;
        public static bool white_passed = false;
        public static bool black_passed = false;
        public static Networker network;
        public static ReplaySystem system;
        public static int myColor;

        public static bool myTurn { get { return myColor == turn; } }
        public static void DoTurn(int color, string action, Vector3 pos, bool byNetwork = false)
        {
            if ((color != myColor) && !byNetwork) return;
            if (action == "turn")
            {
                if (Logic.territory_check)
                {
                    Logic.game_logic.CheckTerritories();
                    Logic.game_logic.CheckTerritories();
                }

                black_passed = false;
                white_passed = false;

                string text_pos = "" + Logic.letters[(int)pos.z] + "," + (Logic.game_logic.board_size + 1 - (int)pos.x);
                if (color == 0)
                    AddUIScrollText("White: " + text_pos, Color.white);
                if (color == 1)
                    AddUIScrollText("Black: " + text_pos, Color.black);
            }

            else if (action == "pass")
            {
                if (color == 0)
                {
                    AddUIScrollText("White passed", Color.white);

                    if (!Logic.in_replay)
                    {
                        white_passed = true;
                        if (black_passed)
                            TurnManager.DoTurn(0, "game over", Vector3.zero);
                    }
                }
                if (color == 1)
                {
                    AddUIScrollText("Black passed", Color.black);

                    if (!Logic.in_replay)
                    {
                        black_passed = true;
                        if (white_passed)
                            TurnManager.DoTurn(0, "game over", Vector3.zero);
                    }
                }
            }

            else if (action == "resign")
            {
                if (Logic.territory_check)
                    Logic.game_logic.CheckTerritories();
                Logic.game_logic.CheckTerritories();

                AddUIScrollText("", Color.black);
                if (color == 0)
                {
                    AddUIScrollText("White resigned", Color.white);
                    AddUIScrollText("Black wins!", Color.green);
                }
                if (color == 1)
                {
                    AddUIScrollText("Black resigned", Color.black);
                    AddUIScrollText("White wins!", Color.green);
                }
            }

            else if (action == "suicide")
                AddUIScrollText("Suicide is forbidden", Color.red);

            else if (action == "ko")
                AddUIScrollText("Ko Rule in Effect", Color.red);

            else if (action == "area points")
            {
                int white_points = (int)pos.x;
                int black_points = (int)pos.y;

                AddUIScrollText("White Area Points: " + white_points, Color.white);
                AddUIScrollText("Black Area Points: " + black_points, Color.black);
            }

            else if (action == "game over")
            {
                AddUIScrollText("", Color.black);
                AddUIScrollText("Game Over!", Color.red);

                if (Logic.territory_check)
                    Logic.game_logic.CheckTerritories();
                Logic.game_logic.CheckTerritories();

                AddUIScrollText("", Color.black);

                int white_pieces = 0;
                int black_pieces = 0;
                foreach (Tile t in Logic.tiles)
                {
                    if (t.piece == null)
                        continue;

                    if (t.piece.color == 0)
                        white_pieces++;
                    else
                        black_pieces++;
                }

                float white_points = Logic.points[0] + Logic.white_area_points + white_pieces + 6.5f;
                int black_points = Logic.points[1] + Logic.black_area_points + black_pieces;

                AddUIScrollText("Captured: " + Logic.points[1], Color.black);
                AddUIScrollText("Black pieces: " + black_pieces, Color.black);
                AddUIScrollText("Total Black Points: " + black_points, Color.black);

                AddUIScrollText("", Color.black);

                AddUIScrollText("Captured: " + Logic.points[0], Color.white);
                AddUIScrollText("White pieces: " + white_pieces, Color.white);
                AddUIScrollText("Total White Points: " + white_points, Color.white);

                AddUIScrollText("", Color.black);

                if (white_points > black_points)
                    AddUIScrollText("White wins!", Color.green);
                if (black_points > white_points)
                    AddUIScrollText("Black wins!", Color.green);

                Logic.game_over = true;
                ReplaySystem.replay_system.replay.black_points = black_points;
                ReplaySystem.replay_system.replay.white_points = white_points;
                ReplaySystem.replay_system.SaveReplay();
            }

            else if (action == "game over replay")
            {

                if (Logic.territory_check)
                    Logic.game_logic.CheckTerritories();
                Logic.game_logic.CheckTerritories();

                int black_points = ReplaySystem.replay_system.replay.black_points;
                float white_points = ReplaySystem.replay_system.replay.white_points;
                AddUIScrollText("", Color.black);
                AddUIScrollText("Game Over!", Color.red);
                AddUIScrollText("", Color.black);
                AddUIScrollText("Black Points: " + black_points, Color.black);
                AddUIScrollText("White Points: " + white_points, Color.white);
                AddUIScrollText("", Color.black);

                if (white_points > black_points)
                    AddUIScrollText("White wins!", Color.green);
                if (black_points > white_points)
                    AddUIScrollText("Black wins!", Color.green);
            }
            if (!byNetwork)
            {
                string json = "[";
                for (int i = 0; i < system.current_turns.Count; i++)
                {
                    if (i == system.current_turns.Count - 1)
                    {
                        json += JsonUtility.ToJson(system.current_turns[i]) + "]";
                    } else
                    {
                        json += JsonUtility.ToJson(system.current_turns[i]) + ", ";
                    }
                }
                network.SocketMessage("game-update", json);
                Debug.Log("send go update");
            }
        }

        public static void AddUIScrollText(string text, Color clr)
        {
            GameObject text_object = GameObject.Instantiate(Resources.Load<GameObject>("Prefabs/UIScrollText"));
            Text t = text_object.GetComponent<Text>();
            t.text = text;
            t.color = clr;
            text_object.transform.SetParent(Logic.scroll_content.transform);
        }

        public static IEnumerator RemoveLastUIScrollText()
        {
            foreach (Transform transform in Logic.scroll_content.transform)
            {
                if (transform == Logic.scroll_content.transform)
                    continue;

                Text t = transform.gameObject.GetComponent<Text>();
                if (t == null || 
                    (!t.text.StartsWith("Black:") && !t.text.StartsWith("White:") && 
                    !t.text.StartsWith("Black passed") && !t.text.StartsWith("White passed") && 
                    !t.text.StartsWith("Black resigned") && !t.text.StartsWith("White resigned")))
                    {
                        GameObject.Destroy(transform.gameObject);
                    }
            }
            yield return new WaitForEndOfFrame();
            if (Logic.scroll_content.transform.childCount > 0)
            {
                Transform last = Logic.scroll_content.transform.GetChild(Logic.scroll_content.transform.childCount - 1);
                GameObject.Destroy(last.gameObject);
            }
        }
    }
}